package org.example.Oh_h1;

import java.util.Arrays;
import java.util.stream.Collectors;

public class BoardFormatter {
    public static String getStringRepresentation(final Board board) {
        final StringBuilder sb = new StringBuilder();

        for (int rowIndex = 0; rowIndex < board.getSize(); rowIndex++) {
            final int[] row = board.getRow(rowIndex);

            final String rowString = Arrays.stream(row).boxed()
                    .map(i -> Integer.toString(i))
                    .collect(Collectors.joining(" | "));

            sb.append(rowString).append("\n");
        }

        return sb.toString();
    }
}

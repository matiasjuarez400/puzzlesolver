package org.example.Oh_h1;

import org.example.Oh_h1.solution_seekers.SameSeriesSolutionSeeker;
import org.example.Oh_h1.solution_seekers.SolutionSeeker;
import org.example.Oh_h1.solution_seekers.SurroundedBySameValueSolutionSeeker;
import org.example.Oh_h1.solution_seekers.TotalElementsInSeriesSolutionSeeker;
import org.example.Oh_h1.solution_seekers.TwoCellWithSameValueSolutionSeeker;

import java.util.Arrays;
import java.util.List;

public class Solver {
    private final List<SolutionSeeker> solutionSeekerList;

    public Solver(final List<SolutionSeeker> solutionSeekerList) {
        this.solutionSeekerList = solutionSeekerList;
    }

    public static Solver withAllSolutionSeekers() {
        final List<SolutionSeeker> solutionSeekers = Arrays.asList(
                new TwoCellWithSameValueSolutionSeeker(),
                new SurroundedBySameValueSolutionSeeker(),
                new TotalElementsInSeriesSolutionSeeker(),
                new SameSeriesSolutionSeeker()
        );

        return new Solver(solutionSeekers);
    }

    public void solve(final Board board) {
        boolean atLeastOneSeekerProgress;

        do {
            atLeastOneSeekerProgress = false;

            for (final SolutionSeeker solutionSeeker : solutionSeekerList) {
                boolean modificationDone;
                do {
                    modificationDone = solutionSeeker.seek(board);

                    if (modificationDone) {
                        atLeastOneSeekerProgress = true;
                    }
                } while (modificationDone);
            }
        } while (atLeastOneSeekerProgress);
    }
}

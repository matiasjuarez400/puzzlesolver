package org.example.Oh_h1.solution_seekers;

import org.example.Oh_h1.Board;

public class SurroundedBySameValueSolutionSeeker extends SolutionSeeker {
    @Override
    public boolean seek(final Board board) {
        final boolean rowProgress = seekIndividualSeries(board, board::getRow, board::setRow, this::seek);
        final boolean colProgress = seekIndividualSeries(board, board::getColumn, board::setColumn, this::seek);

        return rowProgress || colProgress;
    }

    private boolean seek(final int[] series) {
        boolean progress = false;

        for (int i = 1; i < series.length - 1; i++) {
            final int currentValue = series[i];

            if (currentValue == Board.VALUE_EMPTY) {
                final int leftValue = series[i - 1];
                final int rightValue = series[i + 1];

                if (leftValue == rightValue && leftValue != Board.VALUE_EMPTY) {
                    series[i] = Board.getOpposedValue(rightValue);
                    progress = true;
                }
            }
        }

        return progress;
    }
}

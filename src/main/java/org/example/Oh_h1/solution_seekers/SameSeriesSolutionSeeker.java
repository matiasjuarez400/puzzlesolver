package org.example.Oh_h1.solution_seekers;

import org.example.Oh_h1.Board;

import java.util.function.BiConsumer;
import java.util.function.Function;

public class SameSeriesSolutionSeeker extends SolutionSeeker {
    @Override
    public boolean seek(final Board board) {
        final boolean rowSeek = seek(board, board::getRow, board::setRow);
        final boolean colSeek = seek(board, board::getColumn, board::setColumn);

        return rowSeek || colSeek;
    }

    private boolean seek(final Board board,
                         final Function<Integer, int[]> seriesGetter,
                         final BiConsumer<Integer, int[]> boardSetter) {
        boolean progress = false;

        final int[] missingCellsPerSeries = new int[board.getSize()];

        for (int i = 0; i < board.getSize(); i++) {
            final int[] nextSeries = seriesGetter.apply(i);
            final int missingCellsCount = countMissingCells(nextSeries);
            missingCellsPerSeries[i] = missingCellsCount;
        }

        for (int i = 0; i < board.getSize(); i++) {
            final int leftMissingCells = missingCellsPerSeries[i];

            if (leftMissingCells != 2) {
                continue;
            }

            final int[] leftSeries = seriesGetter.apply(i);

            for (int j = 0; j < board.getSize(); j++) {
                if (j == i) {
                    continue;
                }

                final int rightMissingCells = missingCellsPerSeries[j];

                if (rightMissingCells != 0) {
                    continue;
                }

                final int[] rightSeries = seriesGetter.apply(j);

                if (existingCellsMatch(leftSeries, rightSeries)) {
                    for (int k = 0; k < leftSeries.length; k++) {
                        if (leftSeries[k] == Board.VALUE_EMPTY) {
                            leftSeries[k] = Board.getOpposedValue(rightSeries[k]);
                        }
                    }

                    boardSetter.accept(i, leftSeries);
                    progress = true;
                }
            }
        }

        return progress;
    }

    private boolean existingCellsMatch(final int[] leftSeries, final int[] rightSeries) {
        for (int i = 0; i < leftSeries.length; i++) {
            final int leftValue = leftSeries[i];

            if (leftValue == Board.VALUE_EMPTY) {
                continue;
            }

            final int rightCell = rightSeries[i];

            if (leftValue != rightCell) {
                return false;
            }
        }

        return true;
    }

    private int countMissingCells(final int[] series) {
        int acc = 0;

        for (int i : series) {
            if (i == Board.VALUE_EMPTY) {
                acc++;
            }
        }

        return acc;
    }
}

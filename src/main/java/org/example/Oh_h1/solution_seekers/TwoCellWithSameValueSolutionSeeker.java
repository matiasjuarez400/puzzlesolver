package org.example.Oh_h1.solution_seekers;

import org.example.Oh_h1.Board;

public class TwoCellWithSameValueSolutionSeeker extends SolutionSeeker {
    @Override
    public boolean seek(final Board board) {
        final boolean rowProgress = seekIndividualSeries(board, board::getRow, board::setRow, this::seek);
        final boolean colProgress = seekIndividualSeries(board, board::getColumn, board::setColumn, this::seek);

        return rowProgress || colProgress;
    }

    private boolean seek(final int[] series) {
        int lastValue = -1;
        int acc = 0;
        boolean modificationDone = false;

        for (int i = 0; i < series.length; i++) {
            final int nextValue = series[i];

            if (nextValue == lastValue) {
                if (nextValue != Board.VALUE_EMPTY) {
                    acc++;

                    if (acc == 2) {
                        final int rightIndex = i + 1;
                        final int leftIndex = i - 2;
                        final int opposedValue = Board.getOpposedValue(nextValue);

                        if (rightIndex < series.length) {
                            if (series[rightIndex] == Board.VALUE_EMPTY) {
                                series[rightIndex] = opposedValue;
                                modificationDone = true;
                            }
                        }

                        if (leftIndex >= 0) {
                            if (series[leftIndex] == Board.VALUE_EMPTY) {
                                series[leftIndex] = opposedValue;
                                modificationDone = true;
                            }
                        }
                    }
                }
            } else {
                lastValue = nextValue;

                if (lastValue == Board.VALUE_EMPTY) {
                    acc = 0;
                } else {
                    acc = 1;
                }
            }
        }

        return modificationDone;
    }
}

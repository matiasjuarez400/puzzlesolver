package org.example.Oh_h1.solution_seekers;

import org.example.Oh_h1.Board;

public class TotalElementsInSeriesSolutionSeeker extends SolutionSeeker {
    @Override
    public boolean seek(final Board board) {
        final boolean rowProgress = seekIndividualSeries(board, board::getRow, board::setRow, this::seek);
        final boolean colProgress = seekIndividualSeries(board, board::getColumn, board::setColumn, this::seek);

        return rowProgress || colProgress;
    }

    private boolean seek(final int[] series) {
        boolean progress = false;

        final int elementsOfEachTypeBySeries = series.length / 2;

        int redAcc = 0;
        int blueAcc = 0;

        for (int j = 0; j < series.length; j++) {
            if (series[j] == Board.VALUE_RED) {
                redAcc++;
            } else if (series[j] == Board.VALUE_BLUE) {
                blueAcc++;
            }
        }

        // The row is already filled
        if (redAcc + blueAcc == series.length) {
            return false;
        }

        final Integer fillingValueToUse;
        if (redAcc == elementsOfEachTypeBySeries) {
            fillingValueToUse = Board.VALUE_BLUE;
        } else if (blueAcc == elementsOfEachTypeBySeries) {
            fillingValueToUse = Board.VALUE_RED;
        } else {
            fillingValueToUse = null;
        }

        if (fillingValueToUse != null) {
            progress = true;

            for (int i = 0; i < series.length; i++) {
                if (series[i] == Board.VALUE_EMPTY) {
                    series[i] = fillingValueToUse;
                }
            }
        }

        return progress;
    }
}

package org.example.Oh_h1.solution_seekers;

import org.example.Oh_h1.Board;

import java.util.function.BiConsumer;
import java.util.function.Function;

public abstract class SolutionSeeker {
    public abstract boolean seek(final Board board);

    protected boolean seekIndividualSeries(
            final Board board,
            final Function<Integer, int[]> seriesGetter,
            final BiConsumer<Integer, int[]> setter,
            final Function<int[], Boolean> seekerLogic) {
        boolean progress = false;

        for (int i = 0; i < board.getSize(); i++) {
            final int[] series = seriesGetter.apply(i);

            if(seekerLogic.apply(series)) {
                progress = true;
                setter.accept(i, series);
            }
        }

        return progress;
    }
}

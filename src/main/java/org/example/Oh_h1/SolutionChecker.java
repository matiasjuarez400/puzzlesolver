package org.example.Oh_h1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class SolutionChecker {
    public SolutionCheckerResult checkSolution(final Board board) {
        final List<Function<Board, Optional<String>>> analyzers = Arrays.asList(
                this::checkAllCellsAreFilled,
                this::checkNoMoreThanTwoOfSameValue,
                this::checkNoTwoSeriesAreTheSame
        );

        return analyzers.stream()
                .map(analyzer -> analyzer.apply(board))
                .filter(Optional::isPresent)
                .findFirst()
                .map(optional -> SolutionCheckerResult.builder()
                        .isSolved(false)
                        .description(optional.get())
                        .build()
                ).orElse(SolutionCheckerResult.builder()
                        .isSolved(true)
                        .build()
                );
    }

    private Optional<String> checkNoTwoSeriesAreTheSame(final Board board) {
        final Optional<String> rowErrors = checkNoTwoSeriesAreTheSame(board, board::getRow, true);
        final Optional<String> colErrors = checkNoTwoSeriesAreTheSame(board, board::getColumn, false);

        final List<String> errors = new ArrayList<>();

        rowErrors.ifPresent(errors::add);
        colErrors.ifPresent(errors::add);

        if (errors.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(String.join("\n", errors));
    }

    private Optional<String> checkNoTwoSeriesAreTheSame(
            final Board board,
            final Function<Integer, int[]> seriesGetter,
            final boolean isRow) {
        final List<String> errors = new ArrayList<>();

        final boolean[] fullSeries = new boolean[board.getSize()];

        for (int i = 0; i < fullSeries.length; i++) {
            final int[] nextSeries = seriesGetter.apply(i);
            fullSeries[i] = isFull(nextSeries);
        }

        for (int i = 0; i < board.getSize(); i++) {
            if (!fullSeries[i]) {
                continue;
            }

            final int[] leftSeries = seriesGetter.apply(i);

            for (int j = i + 1; j < board.getSize(); j++) {
                if (!fullSeries[j]) {
                    continue;
                }

                final int[] rightSeries = seriesGetter.apply(j);

                if (wholeSeriesMatch(leftSeries, rightSeries)) {
                    final String model = "The following two %s are the same: %d, %d";
                    final String orientation = isRow ? "row" : "column";

                    final String description = String.format(model, orientation, i, j);

                    errors.add(description);
                }
            }
        }

        if (errors.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(String.join("\n", errors));
    }

    private boolean wholeSeriesMatch(final int[] leftSeries, final int[] rightSeries) {
        for (int i = 0; i < leftSeries.length; i++) {
            if (leftSeries[i] != rightSeries[i]) {
                return false;
            }
        }

        return true;
    }

    private boolean isFull(final int[] series) {
        int acc = 0;
        for (int i : series) {
            if (i != Board.VALUE_EMPTY) {
                acc++;
            }
        }

        return acc == series.length;
    }

    private Optional<String> checkNoMoreThanTwoOfSameValue(final Board board) {
        final Optional<String> rowErrors = checkNoMoreThanTwoOfSameValue(board, board::getRow, true);
        final Optional<String> colErrors = checkNoMoreThanTwoOfSameValue(board, board::getColumn, false);

        final List<String> errors = new ArrayList<>();

        rowErrors.ifPresent(errors::add);
        colErrors.ifPresent(errors::add);

        if (errors.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(String.join("\n", errors));
    }

    private Optional<String> checkNoMoreThanTwoOfSameValue(
            final Board board,
            final Function<Integer, int[]> seriesGetter,
            final boolean isRow) {
        final List<String> errors = new ArrayList<>();

        for (int i = 0; i < board.getSize(); i++) {
            final int[] nextSeries = seriesGetter.apply(i);

            int acc = 0;
            int currentValue = -1;

            for (int j = 0; j < nextSeries.length; j++) {
                final int nextValue = nextSeries[j];

                if (currentValue == nextValue) {
                    acc++;
                } else {
                    if (acc > 2) {
                       final int errorRightLimit = j - 1;
                       final int errorLeftLimit = errorRightLimit - acc - 1;

                       final String model = "More than 2 consecutive cells with same value at %s %d. Positions [%s]";
                       final String orientation = isRow ? "row": "column";
                       final String position = String.format("%d - %d", errorLeftLimit, errorRightLimit);

                       final String description = String.format(model, orientation, i, position);

                       errors.add(description);
                    }

                    acc = 1;
                    currentValue = nextValue;
                }
            }
        }

        if (errors.isEmpty()) {
            return Optional.empty();
        }

        final String errorsString = String.join("\n", errors);

        return Optional.of(errorsString);
    }

    private Optional<String> checkAllCellsAreFilled(final Board board) {
        final List<String> missingCells = new ArrayList<>();

        for (int row = 0; row < board.getSize(); row++) {
            for (int col = 0; col < board.getSize(); col++) {
                if (board.cellIsEmpty(row, col)) {
                    missingCells.add(String.format("[%d-%d]", row, col));
                }
            }
        }

        if (missingCells.isEmpty()) {
            return Optional.empty();
        }

        final String missingCellsMessage = String.join(" , ", missingCells);

        return Optional.of(String.format("The following cells are empty: {%s}", missingCellsMessage));
    }
}

package org.example.Oh_h1;

public class Board {
    private final int[][] boardMatrix;
    public static final int VALUE_EMPTY = 0;
    public static final int VALUE_RED = 1;
    public static final int VALUE_BLUE = 2;

    public Board(final int[][] inputBoardStatus) {
        if (inputBoardStatus.length == 0 || inputBoardStatus[0].length == 0) {
            throw new IllegalArgumentException("The board size can not be zero");
        }

        if (inputBoardStatus.length != inputBoardStatus[0].length) {
            throw new IllegalArgumentException("The board must have the same amount of rows and columns");
        }

        this.boardMatrix = new int[inputBoardStatus.length][inputBoardStatus.length];

        for (int row = 0; row < this.boardMatrix.length; row++) {
            for (int col = 0; col < this.boardMatrix.length; col++) {
                final int nextElement = inputBoardStatus[row][col];

                checkCellValue(nextElement);

                this.boardMatrix[row][col] = nextElement;
            }
        }
    }

    public int getCell(final int row, final int col) {
        return boardMatrix[row][col];
    }

    public int[] getRow(final int rowIndex) {
        final int[] row = new int[boardMatrix.length];

        for (int col = 0; col < row.length; col++) {
            row[col] = boardMatrix[rowIndex][col];
        }

        return row;
    }

    public int[] getColumn(final int columnIndex) {
        final int[] column = new int[boardMatrix.length];

        for (int row = 0; row < column.length; row++) {
            column[row] = boardMatrix[row][columnIndex];
        }

        return column;
    }

    public boolean cellIsEmpty(final int row, final int col) {
        return boardMatrix[row][col] == VALUE_EMPTY;
    }

    public void setCellValue(final int row, final int col, final int value) {
        checkCellValue(value);

        this.boardMatrix[row][col] = value;
    }

    public void setRow(final int rowIndex, final int[] row) {
        if (row.length != getSize()) {
            throw new IllegalArgumentException("Input row is of different size than the board");
        }

        if (!rowExists(rowIndex)) {
            throw new IllegalArgumentException("The row index is invalid: " + rowIndex);
        }

        for (int col = 0; col < row.length; col++) {
            this.boardMatrix[rowIndex][col] = row[col];
        }
    }

    public void setColumn(final int columnIndex, final int[] column) {
        if (column.length != getSize()) {
            throw new IllegalArgumentException("Input column is of different size than the board");
        }

        if (!colExists(columnIndex)) {
            throw new IllegalArgumentException("The column index is invalid: " + columnIndex);
        }

        for (int row = 0; row < column.length; row++) {
            this.boardMatrix[row][columnIndex] = column[row];
        }
    }

    public void clearCellValue(final int row, final int col) {
        this.boardMatrix[row][col] = 0;
    }

    public boolean cellExists(final int row, final int col) {
        return rowExists(row) && colExists(col);
    }

    public boolean rowExists(final int row) {
        return row >= 0 && row < boardMatrix.length;
    }

    public boolean colExists(final int col) {
        return col >= 0 && col < boardMatrix.length;
    }

    public int getSize() {
        return boardMatrix.length;
    }

    public static int getOpposedValue(final int value) {
        checkCellValue(value);

        if (value == Board.VALUE_EMPTY) {
            throw new IllegalArgumentException("The value represents a clear cell");
        }

        if (value == Board.VALUE_RED) {
            return Board.VALUE_BLUE;
        } else {
            return Board.VALUE_RED;
        }
    }

    private static void checkCellValue(final int cellValue) {
        if (cellValue < 0 || cellValue > 2) {
            throw new IllegalArgumentException("The board can only have elements 0, 1 and 2");
        }
    }

    @Override
    public String toString() {
        return BoardFormatter.getStringRepresentation(this);
    }
}

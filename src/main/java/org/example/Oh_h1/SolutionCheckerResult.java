package org.example.Oh_h1;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class SolutionCheckerResult {
    private final boolean isSolved;
    private final String description;
}

package org.example.Oh_h1;

public class App {
    public static void main(String[] args) {
        final int[][] boardMatrix = matrix001();

        solve(boardMatrix);
    }

    private static int[][] matrix001() {
        return new int[][]{
                {0, 1, 0, 2, 0, 2},
                {0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 0},
                {0, 0, 0, 1, 1, 0},
                {0, 2, 0, 0, 0, 0}
        };
    }

    private static void solve(final int[][] boardMatrix) {
        final Board board = new Board(boardMatrix);

        printBoard(board);

        final Solver solver = Solver.withAllSolutionSeekers();
        solver.solve(board);

        printBoard(board);

        final SolutionChecker solutionChecker = new SolutionChecker();
        final SolutionCheckerResult result = solutionChecker.checkSolution(board);

        System.out.println(result);
    }

    private static void printBoard(final Board board) {
        System.out.println(BoardFormatter.getStringRepresentation(board));
    }
}

package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class EightQueensSolver {
    public static void main(String[] args) {
        EightQueensSolver solver = new EightQueensSolver();

        List<int[][]> validSolutions = solver.solve();

        System.out.println("Solutions: " + validSolutions.size());

        for (int[][] solution : validSolutions) {
            System.out.println(solver.buildBoardRepresentation(solution));
            System.out.println("------------------------------------\n");
        }
    }

    public List<int[][]> solve() {
        List<int[][]> validSolutions = new ArrayList<>();
        int[][] board = new int[8][8];
        Memory memory = new Memory();

        findSolutions(validSolutions, board, memory);

        return validSolutions;
    }

    private void findSolutions(List<int[][]> validSolutions, int[][] board, Memory memory) {
        //printBoard(board);
        if (memory.queenCount() == board.length) {
            validSolutions.add(copyBoard(board));
            return;
        }

        int[] lastInsertedQueen = memory.getLastQueen();

        int currentRow = lastInsertedQueen == null ? 0 : lastInsertedQueen[0] + 1;

        for (int col = 0; col < board.length; col++) {
            if (memory.queenCount() > 0 && memory.getLastQueen()[0] == currentRow) {
                int[] removedQueen = memory.removeLastQueen();
                board[removedQueen[0]][removedQueen[1]] = 0;
                //printBoard(board);
            }

            if (canInsertQueen(board, currentRow, col)) {
                board[currentRow][col] = 1;
                memory.addQueen(currentRow, col);
                findSolutions(validSolutions, board, memory);
            }
        }

        if (memory.queenCount() > 0 && memory.getLastQueen()[0] == currentRow) {
            int[] removedQueen = memory.removeLastQueen();
            board[removedQueen[0]][removedQueen[1]] = 0;
            //printBoard(board);
        }
    }

    private int[][] copyBoard(int[][] board) {
        int[][] copy = new int[board.length][board.length];

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                copy[i][j] = board[i][j];
            }
        }

        return copy;
    }

    private static class Memory {
        private Stack<int[]> insertedQueens = new Stack<>();

        public void addQueen(int row, int col) {
            insertedQueens.push(new int[] {row, col});
        }

        public int[] removeLastQueen() {
            if (insertedQueens.empty()) return null;
            return insertedQueens.pop();
        }

        public int[] getLastQueen() {
            if (insertedQueens.empty()) return null;
            return insertedQueens.peek();
        }

        public int queenCount() {
            return insertedQueens.size();
        }
    }

    public boolean canInsertQueen(int[][] board, int row, int col) {
        return analyzeInDirection(0, col, 1, 0, board) &&
                analyzeInDirection(row, 0, 0, 1, board) &&
                analyzeInDirection(row, col, -1, -1, board) &&
                analyzeInDirection(row, col, -1, 1, board) &&
                analyzeInDirection(row, col, 1, -1, board) &&
                analyzeInDirection(row, col, 1, 1, board);
    }

    private void printBoard(int[][] board) {
        System.out.println(buildBoardRepresentation(board));
    }

    public String buildBoardRepresentation(int[][] board) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i <= board.length; i++) {
            for (int j = 0; j <= board.length; j++) {
                String nextChar;
                if (i == 0) {
                    if (j == 0) {
                        nextChar = "X";
                    } else {
                        nextChar = Integer.toString(j - 1);
                    }
                } else {
                    if (j == 0) {
                        nextChar = Integer.toString(i - 1);
                    } else {
                        nextChar = board[i - 1][j - 1] == 1 ? "X" : " ";
                    }
                }


                sb.append(" ").append(nextChar).append(" |");
            }
            sb.append("\n");
        }

        return sb.toString();
    }

    private boolean analyzeInDirection(int iRow, int iCol, int rowStep, int colStep, int[][] board) {
        int currentRow;
        int currentCol;

        for (int step = 0; step < board.length; step++) {
            currentRow = iRow + step * rowStep;
            currentCol = iCol + step * colStep;

            if (currentRow < 0 || currentRow >= board.length || currentCol < 0 || currentCol >= board[currentRow].length) break;

            if (board[currentRow][currentCol] == 1) return false;
        }

        return true;
    }
}

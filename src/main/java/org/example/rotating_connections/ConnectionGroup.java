package org.example.rotating_connections;

class ConnectionGroup {
    private final Connection connection;
    private final ConnectionConstraint connectionConstraint;

    public ConnectionGroup(Connection connection, ConnectionConstraint connectionConstraint) {
        this.connection = connection;
        this.connectionConstraint = connectionConstraint;
    }

    public ConnectionGroup(Color[][] connection, Color[][] constraint) {
        this(connection, constraint, 0);
    }

    public ConnectionGroup(Color[][] connection, Color[][] constraint, int initialRotation) {
        this.connection = new Connection(connection, initialRotation);
        this.connectionConstraint = new ConnectionConstraint(constraint);
    }

    public boolean isConstraintAligned() {
        for (int row = 0; row < connectionConstraint.getRows(); row++) {
            for (int col = 0; col < connectionConstraint.getCols(); col++) {
                Color constraintColor = connectionConstraint.getConstraint(row, col);
                if (constraintColor == null) continue;

                Color connectionColor = connection.getConnection(row, col);

                if (connectionColor != constraintColor) return false;
            }
        }

        return true;
    }

    public void rotateConnection() {
        connection.rotate();
    }

    public void antiRotateConnection() {
        connection.antiRotate();
    }

    public Color getRightConnectionColor() {
        return connection.getConnection(1, 2);
    }

    public Color getLeftConnectionColor() {
        return connection.getConnection(1, 0);
    }

    public Color getUpConnectionColor() {
        return connection.getConnection(0, 1);
    }

    public Color getDownConnectionColor() {
        return connection.getConnection(2, 1);
    }

    public int getConnectionRotation() {
        return connection.getRelativeRotation();
    }
}

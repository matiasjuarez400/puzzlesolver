package org.example.rotating_connections;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Solver {
    public List<Coordinate> solve(ConnectionMatrix connectionMatrix) {

        for (int connectionUseLimit = 2; connectionUseLimit < 5; connectionUseLimit++) {
            Memory memory = new Memory(3);

            boolean canSolve = doSolve(connectionMatrix, memory);

            if (canSolve) return memory.steps;
        }

        throw new IllegalStateException("Can not solve the connection matrix");
    }

    private boolean doSolve(ConnectionMatrix matrix, Memory memory) {
        if (matrix.isValidMatrix()) return true;

        // Already visited position
        final String positionSignature = matrix.getConnectionsRotations();
        if (memory.isVisitedPosition(positionSignature)) return false;
        memory.addVisitedPosition(positionSignature);

        for (int row = 0; row < matrix.getMatrix().length; row++) {
            for (int col = 0; col < matrix.getMatrix()[0].length; col++) {
                if (!memory.canUseConnection(row, col)) continue;

                Coordinate step = new Coordinate(row, col);
                matrix.rotate(row, col);

                memory.addStep(step);

                boolean solved = doSolve(matrix, memory);

                if (solved) return true;

                memory.removeLastStep();
                matrix.antiRotate(row, col);
            }
        }

        return false;
    }

    private static class Memory {
        private final List<Coordinate> steps;
        private final Set<String> visitedPositions;
        private final int[][] usesByConnection;
        private final int connectionUseLimit;

        public Memory(int connectionUseLimit) {
            this.connectionUseLimit = connectionUseLimit;
            this.steps = new ArrayList<>();
            this.visitedPositions = new HashSet<>();
            this.usesByConnection = new int[3][3];
        }

        public void addStep(Coordinate step) {
            this.steps.add(step);
            this.usesByConnection[step.getRow()][step.getCol()]++;
        }

        public void removeLastStep() {
            if (this.steps.isEmpty()) return;
            Coordinate lastStep = this.steps.remove(this.steps.size() - 1);
            int newValue = this.usesByConnection[lastStep.getRow()][lastStep.getCol()]--;

            if (newValue < 0) {
                throw new IllegalStateException(String.format("Connection at %s has negative usages", lastStep));
            }
        }

        public boolean addVisitedPosition(String position) {
            return visitedPositions.add(position);
        }

        public boolean isVisitedPosition(String position) {
            return visitedPositions.contains(position);
        }

        public int getUsesByConnection(int row, int col) {
            return this.usesByConnection[row][col];
        }

        public boolean canUseConnection(int row, int col) {
            return this.usesByConnection[row][col] < this.connectionUseLimit;
        }
    }
}

package org.example.rotating_connections;

import lombok.Getter;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class ConnectionMatrix {
    @Getter
    private final ConnectionGroup[][] matrix;
    private int nextRow = 0;

    public ConnectionMatrix(int rows, int cols) {
        this.matrix = new ConnectionGroup[rows][cols];
    }

    public ConnectionMatrix addRow(ConnectionGroup... connectionGroups) {
        if (nextRow >= matrix.length) {
            throw new IllegalStateException("The connection matrix has reached the maximum number of rows");
        }

        if (connectionGroups.length != matrix[0].length) {
            throw new IllegalArgumentException(
                    String.format("The row does not have the right size. Expected: %s. Got: %s \n",
                            connectionGroups.length, matrix[0].length));
        }

        for (int col = 0; col < connectionGroups.length; col++) {
            matrix[nextRow][col] = connectionGroups[col];
        }

        nextRow++;

        return this;
    }

    public ConnectionMatrix rotate(Coordinate coordinate) {
        return rotate(coordinate.getRow(), coordinate.getCol());
    }

    public ConnectionMatrix rotate(int row, int col) {
        return doRotation(row, col, ConnectionGroup::rotateConnection);
    }

    public ConnectionMatrix antiRotate(int row, int col) {
        return doRotation(row, col, ConnectionGroup::antiRotateConnection);
    }

    private ConnectionMatrix doRotation(int row, int col, Consumer<ConnectionGroup> consumer) {
        Stream.of(
                getGroup(row - 1, col),
                getGroup(row, col + 1),
                getGroup(row + 1, col),
                getGroup(row, col - 1),
                getGroup(row, col)
        ).filter(Objects::nonNull)
        .forEach(consumer);

        return this;
    }

    public boolean isValidMatrix() {
        ConnectionGroup first;
        ConnectionGroup second;

        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                if (col < matrix[0].length - 1) {
                    first = matrix[row][col];
                    second = matrix[row][col + 1];

                    if (first.getRightConnectionColor() != second.getLeftConnectionColor()) {
                        //System.out.printf("Connection group at [%s, %s] is not aligned with right neighbour \n", row, col);
                        return false;
                    }
                }

                if (row < matrix.length - 1) {
                    first = matrix[row][col];
                    second = matrix[row + 1][col];

                    if (first.getDownConnectionColor() != second.getUpConnectionColor()) {
                        //System.out.printf("Connection group at [%s, %s] is not aligned with below neighbour \n", row, col);
                        return false;
                    }
                }

                if (!matrix[row][col].isConstraintAligned()) {
                    //System.out.printf("Connection group at [%s, %s] is not aligned with constraints \n", row, col);
                    return false;
                }
            }
        }

        return true;
    }

    private ConnectionGroup getGroup(int row, int col) {
        if (row < 0 || col < 0 || row >= matrix.length || col >= matrix[0].length) return null;

        return matrix[row][col];
    }

    public String getConnectionsRotations() {
        StringBuilder sb = new StringBuilder();
        for (ConnectionGroup[] connectionGroups : matrix) {
            for (ConnectionGroup connectionGroup : connectionGroups) {
                sb.append(connectionGroup.getConnectionRotation());
            }
        }

        return sb.toString();
    }
}

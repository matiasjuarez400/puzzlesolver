package org.example.rotating_connections;

import lombok.Getter;

@Getter
public class Coordinate {
    private final int row;
    private final int col;

    public Coordinate(int row, int col) {
        this.row = row;
        this.col = col;
    }

    @Override
    public String toString() {
        return String.format("{%s, %s}", row, col);
    }
}

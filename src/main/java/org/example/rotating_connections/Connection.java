package org.example.rotating_connections;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

class Connection {
    private final Color[][] connections;
    @Getter
    private final int rows;
    @Getter
    private final int cols;
    private final List<Coordinate> rotationOrder;
    private final List<Coordinate> antiRotationOrder;
    @Getter
    private int relativeRotation;

    public Connection(Color[][] connections) {
        this.connections = connections;
        this.rows = connections.length;
        this.cols = connections[0].length;
        rotationOrder = buildRotationOrder();
        antiRotationOrder = new ArrayList<>(rotationOrder);
        Collections.reverse(antiRotationOrder);
    }

    public Connection(Color[][] connections, int initialRotations) {
        this(connections);
        rotate(initialRotations);
    }

    public Color getConnection(int row, int col) {
        return connections[row][col];
    }

    public void rotate(int times) {
        int effectiveTimes = times % 4;
        doRotation(rotationOrder, effectiveTimes);
        this.relativeRotation = (this.relativeRotation + effectiveTimes) % 4;
    }

    public void rotate() {
        rotate(1);
    }

    public void antiRotate(int times) {
        int effectiveTimes = times % 4;
        doRotation(antiRotationOrder, effectiveTimes);
        this.relativeRotation -= effectiveTimes;
        if (this.relativeRotation < 0) this.relativeRotation += 4;
    }

    public void antiRotate() {
        antiRotate(1);
    }

    private void doRotation(List<Coordinate> rotationList, int times) {
        for (int i = 0; i < times; i++) doRotation(rotationList);
    }

    private void doRotation(List<Coordinate> rotationList) {
        Queue<Color> colorQueue = new LinkedList<>();

        for (int i = 0; i < rotationList.size(); i++) {
            if (colorQueue.size() < 2) {
                Coordinate origin = rotationList.get(i);
                Coordinate target = rotationList.get((i + 2) % rotationList.size());
                Color originColor = connections[origin.getRow()][origin.getCol()];
                Color targetColor = connections[target.getRow()][target.getCol()];

                connections[target.getRow()][target.getCol()] = originColor;
                colorQueue.add(targetColor);
            } else {
                Coordinate target = rotationList.get((i + 2) % rotationList.size());
                Color targetColor = connections[target.getRow()][target.getCol()];

                connections[target.getRow()][target.getCol()] = colorQueue.poll();
                colorQueue.add(targetColor);
            }
        }

    }

    private List<Coordinate> buildRotationOrder() {
        return Arrays.asList(
                new Coordinate(0, 0), new Coordinate(0, 1), new Coordinate(0, 2),
                new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(2, 1),
                new Coordinate(2, 0), new Coordinate(1, 0)
        );
    }
}

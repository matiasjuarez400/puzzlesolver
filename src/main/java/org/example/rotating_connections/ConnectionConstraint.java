package org.example.rotating_connections;

import lombok.Getter;

class ConnectionConstraint {
    private final Color[][] constraintMatrix;
    @Getter
    private final int rows;
    @Getter
    private final int cols;

    public ConnectionConstraint(Color[][] constraintMatrix) {
        this.constraintMatrix = constraintMatrix;
        this.rows = constraintMatrix.length;
        this.cols = constraintMatrix[0].length;
    }

    public Color getConstraint(int row, int col) {
        return constraintMatrix[row][col];
    }
}

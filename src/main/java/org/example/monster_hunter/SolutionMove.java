package org.example.monster_hunter;

import lombok.Getter;

@Getter
public class SolutionMove {
    private int row;
    private int col;

    public SolutionMove(int row, int col) {
        this.row = row;
        this.col = col;
    }

    @Override
    public String toString() {
        return "{"+ row + "-" + col + "}";
    }
}

package org.example.monster_hunter;

import lombok.Getter;

import java.util.Objects;

@Getter
class Piece {
    private final int row;
    private final int col;
    private final PieceType pieceType;

    public Piece(int row, int col, PieceType pieceType) {
        this.row = row;
        this.col = col;
        this.pieceType = pieceType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Piece piece = (Piece) o;
        return row == piece.row &&
                col == piece.col &&
                pieceType == piece.pieceType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, col, pieceType);
    }
}

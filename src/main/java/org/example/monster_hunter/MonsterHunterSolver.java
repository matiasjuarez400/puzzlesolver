package org.example.monster_hunter;

import java.util.ArrayList;
import java.util.List;

public class MonsterHunterSolver {
    /*
     * When set to true, the solver will explore switching moves first, causing the hunter to teleport
     * more frequently in the final solution.
     */
    private final boolean shadowAssassinMode;

    public MonsterHunterSolver(boolean shadowAssassinMode) {
        this.shadowAssassinMode = shadowAssassinMode;
    }

    public MonsterHunterSolver() {
        this(false);
    }

    public List<SolutionMove> solve(int[][] rawPuzzle) {
        Puzzle puzzle = new Puzzle(rawPuzzle);

        List<Move> solutionMoves = new ArrayList<>();

        boolean canSolve = doSolve(puzzle, true, solutionMoves);

        List<SolutionMove> solutionMoveList = new ArrayList<>();

        if (canSolve) {
            for (Move move : solutionMoves) {
                solutionMoveList.add(new SolutionMove(
                   move.getHunterEndPosition().getRow(),
                   move.getHunterEndPosition().getCol()
                ));
            }
        }

        return solutionMoveList;
    }

    private boolean doSolve(Puzzle puzzle, boolean switchingMovesAllowed, List<Move> solution) {
        if (puzzle.isSolved()) return true;

        PuzzleAnalyzer puzzleAnalyzer = new PuzzleAnalyzer(puzzle);
        List<Move> possibleMoves = puzzleAnalyzer.findAllPossibleMoves(!shadowAssassinMode);

        for (Move move : possibleMoves) {
            if (!switchingMovesAllowed && move.isSwitchingMove()) continue;

            Puzzle puzzleCopy = puzzle.copy();
            puzzleCopy.processMove(move);

            solution.add(move);
            // Allow further switching moves if the move is a killing move
            boolean isSolved = doSolve(puzzleCopy, move.isKillingMove(), solution);

            if (isSolved) return true;

            solution.remove(move);
        }

        return false;
    }
}

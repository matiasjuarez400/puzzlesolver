package org.example.monster_hunter;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
class Puzzle {
    List<Piece> monsters;
    Piece hunter;
    PieceType[][] board;

    public Puzzle(int[][] puzzle) {
        board = new PieceType[puzzle.length][puzzle[0].length];
        monsters = new ArrayList<>();

        for (int row = 0; row < puzzle.length; row++) {
            for (int col = 0; col < puzzle[0].length; col++) {
                switch (puzzle[row][col]) {
                    case 1: {
                        hunter = new Piece(row, col, PieceType.HUNTER);
                        board[row][col] = PieceType.HUNTER;
                        break;
                    }
                    case 2: {
                        Piece newMonster = new Piece(row, col, PieceType.MONSTER);
                        monsters.add(newMonster);
                        board[row][col] = PieceType.MONSTER;
                        break;
                    }
                }
            }
        }
    }

    private Puzzle(Puzzle other) {
        this.board = new PieceType[other.board.length][other.board[0].length];

        for (int row = 0; row < this.board.length; row++) {
            for (int col = 0; col < this.board[0].length; col++) {
                this.board[row][col] = other.board[row][col];
            }
        }

        this.monsters = new ArrayList<>(other.monsters);
        this.hunter = other.hunter;
    }

    public Puzzle copy() {
        return new Puzzle(this);
    }

    public void processMove(Move move) {
        if (move.isKillingMove()) {
            Piece monsterPosition = move.getMonsterInitialPosition();
            this.board[monsterPosition.getRow()][monsterPosition.getCol()] = null;
            this.board[hunter.getRow()][hunter.getCol()] = null;
            this.board[move.getHunterEndPosition().getRow()][move.getHunterEndPosition().getCol()] = PieceType.HUNTER;
            this.monsters.remove(monsterPosition);
            this.hunter = move.getHunterEndPosition();
        } else {
            this.hunter = move.getHunterEndPosition();
            this.monsters.remove(move.getMonsterInitialPosition());

            Piece monsterNewPosition = move.getMonsterEndPosition();
            this.monsters.add(monsterNewPosition);
            this.board[move.getMonsterInitialPosition().getRow()][move.getMonsterInitialPosition().getCol()] = PieceType.HUNTER;
            this.board[move.getHunterInitialPosition().getRow()][move.getHunterInitialPosition().getCol()] = PieceType.MONSTER;
        }
    }

    public boolean isSolved() {
        return monsters.isEmpty();
    }
}

package org.example.monster_hunter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class PuzzleAnalyzer {
    private final Puzzle puzzle;

    public PuzzleAnalyzer(Puzzle puzzle) {
        this.puzzle = puzzle;
    }

    public List<Move> findAllPossibleMoves(boolean prioritizeKillingMoves) {
        List<Move> allMoves = new ArrayList<>();

        if (prioritizeKillingMoves) {
            allMoves.addAll(findKillingMoves());
            allMoves.addAll(findSwitchingMoves());
        } else {
            allMoves.addAll(findSwitchingMoves());
            allMoves.addAll(findKillingMoves());
        }

        return allMoves;
    }

    private List<Move> findSwitchingMoves() {
        List<Move> switchingMoves = new ArrayList<>();
        for (Piece piece : puzzle.getMonsters()) {
            switchingMoves.add(new Move(
                    new Piece(puzzle.getHunter().getRow(), puzzle.getHunter().getCol(), PieceType.HUNTER),
                    new Piece(piece.getRow(), piece.getCol(), PieceType.HUNTER),
                    new Piece(piece.getRow(), piece.getCol(), PieceType.MONSTER),
                    new Piece(puzzle.getHunter().getRow(), puzzle.getHunter().getCol(), PieceType.MONSTER)
            ));
        }

        return switchingMoves;
    }

    private List<Move> findKillingMoves() {
        return Stream.of(
                findKillingMoveInDirection(-1, -1),
                findKillingMoveInDirection(-1, 0),
                findKillingMoveInDirection(-1, 1),
                findKillingMoveInDirection(0, 1),
                findKillingMoveInDirection(1, 1),
                findKillingMoveInDirection(1, 0),
                findKillingMoveInDirection(1, -1),
                findKillingMoveInDirection(0, -1)
        )
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private Move findKillingMoveInDirection(int rowStep, int colStep) {
        int hunterRow = puzzle.getHunter().getRow();
        int hunterCol = puzzle.getHunter().getCol();

        int monsterRow = hunterRow + rowStep;
        int monsterCol = hunterCol + colStep;

        // Check if there is a monster to kill
        if (!isValidPosition(monsterRow, monsterCol) || puzzle.getBoard()[monsterRow][monsterCol] == null) return null;

        int hunterFinalRow = hunterRow + rowStep * 2;
        int hunterFinalCol = hunterCol + colStep * 2;

        // If the final cell is occupied, the hunter can't go there
        if (!isValidPosition(hunterFinalRow, hunterFinalCol) || puzzle.getBoard()[hunterFinalRow][hunterFinalCol] != null) return null;

        return new Move(
                new Piece(hunterRow, hunterCol, PieceType.HUNTER),
                new Piece(hunterFinalRow, hunterFinalCol, PieceType.HUNTER),
                new Piece(monsterRow, monsterCol, PieceType.MONSTER),
                null
        );
    }

    private boolean isValidPosition(int row, int col) {
        return row >= 0 && row < puzzle.getBoard().length &&
                col >= 0 && col < puzzle.getBoard()[0].length;
    }
}

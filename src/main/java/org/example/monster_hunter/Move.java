package org.example.monster_hunter;

import lombok.Getter;

@Getter
class Move {
    private final Piece hunterInitialPosition;
    private final Piece hunterEndPosition;
    private final Piece monsterInitialPosition;
    private final Piece monsterEndPosition;

    public Move(Piece hunterInitialPosition, Piece hunterEndPosition, Piece monsterInitialPosition, Piece monsterEndPosition) {
        this.hunterInitialPosition = hunterInitialPosition;
        this.hunterEndPosition = hunterEndPosition;
        this.monsterInitialPosition = monsterInitialPosition;
        this.monsterEndPosition = monsterEndPosition;
    }

    public boolean isKillingMove() {
        return monsterEndPosition == null;
    }

    public boolean isSwitchingMove() {
        return monsterEndPosition != null;
    }
}

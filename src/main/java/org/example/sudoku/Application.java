package org.example.sudoku;

public class Application {
    public static void main(String[] args) {
        final int[][] board = extreme1();

        final Application application = new Application();
        application.runSolver(board);
    }

    private void runSolver(final int[][] board) {
        final SudokuPrinter sudokuPrinter = new SudokuPrinter();

        final SudokuBoard sudokuBoard = new SudokuBoard(board, 3);

        System.out.println("Initial puzzle:");
        System.out.println(sudokuPrinter.print(sudokuBoard));

        final SudokuSolver sudokuSolver = new SudokuSolver();

        System.out.println("Solution found: " + sudokuSolver.solve(sudokuBoard));
        System.out.println(sudokuPrinter.print(sudokuBoard));
    }

    private static int[][] extreme1() {
        return new int[][] {
                { 0,  0,  0,  0, -8,  0,  0,  0, -9},
                { 0, -6,  0, -9,  0,  0, -1, -8,  0},
                { 0,  0, -4,  0, -3,  0,  0,  0,  0},
                {-1,  0,  0, -5,  0, -4,  0,  0, -6},
                { 0,  0,  0, -3,  0, -7, -5, -4,  0},
                { 0,  0,  0,  0,  0,  0,  0,  0,  0},
                {-5,  0, -7,  0,  0,  0,  0, -1,  0},
                {-8, -4,  0,  0,  0, -3,  0,  0,  0},
                { 0,  0, -9,  0,  0,  0, -7,  0, -2}
        };
    }

    private static int[][] board1() {
        return new int[][] {
            {-2, -1, -8,  0,  0, -5,  0,  0,  0},
            {-6, -5, -4,  0,  0, -3, -9,  0,  0},
            { 0,  0,  0,  0,  0,  0,  0, -5,  0},
            { 0, -8,  0,  0, -5,  0,  0, -4,  0},
            { 0,  0,  0,  0,  0, -8,  0, -9, -2},
            {-1,  0,  0, -6,  0,  0, -8, -7, -3},
            { 0, -6, -3, -9, -7, -4,  0, -1,  0},
            {-7,  0, -1,  0, -3,  0, -4,  0,  0},
            { 0,  0,  0,  0,  0, -1,  0,  0,  0}
        };
    }
}

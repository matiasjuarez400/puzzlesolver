package org.example.sudoku;

public class SudokuSolver {
    public boolean solve(final SudokuBoard sudokuBoard) {
        final SudokuBoardAnalyzer analyzer = new SudokuBoardAnalyzer(sudokuBoard);

        return runNextStep(sudokuBoard, analyzer);
    }

    private boolean runNextStep(final SudokuBoard sudokuBoard, final SudokuBoardAnalyzer analyzer) {
        if (analyzer.isSolvedBoard()) {
            return true;
        }

        int nextStepRow = -1;
        int nextStepCol = -1;

        for (int row = 0; row < sudokuBoard.getBoardLength(); row++) {
            for (int col = 0; col < sudokuBoard.getBoardLength(); col++) {
                final int currentValue = sudokuBoard.getValue(row, col);

                if (currentValue == 0 && !sudokuBoard.isReadOnly(row, col)) {
                    nextStepRow = row;
                    nextStepCol = col;
                    break;
                }
            }

            if (nextStepRow != -1) break;
        }

        for (int nextValue = 1; nextValue <= sudokuBoard.getBoardLength(); nextValue++) {
            if (analyzer.isValidMove(nextStepRow, nextStepCol, nextValue)) {
                sudokuBoard.setValue(nextStepRow, nextStepCol, nextValue);
                boolean nextStepExecutionResult = runNextStep(sudokuBoard, analyzer);

                if (nextStepExecutionResult) {
                    return true;
                } else {
                    sudokuBoard.setValue(nextStepRow, nextStepCol, 0);
                }
            }
        }

        return false;
    }
}

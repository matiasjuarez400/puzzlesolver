package org.example.sudoku;

public class SudokuPrinter {
    public String print(final SudokuBoard sudokuBoard) {
        final StringBuilder sb = new StringBuilder();

        for (int row = 0; row < sudokuBoard.getBoardLength(); row++) {
            for (int col = 0; col < sudokuBoard.getBoardLength(); col++) {
                final int nextValue = sudokuBoard.getValue(row, col);

                sb.append(" ").append(nextValue).append(" ");

                if ((col + 1) % sudokuBoard.getAreaLength() == 0) {
                    sb.append("|");
                }
            }

            sb.append("\n");

            if ((row + 1) % sudokuBoard.getAreaLength() == 0) {
                for (int col = 0; col < sudokuBoard.getBoardLength(); col++) {
                    sb.append("---");

                    if (col % sudokuBoard.getAreaLength() == 0) {
                        sb.append("-");
                    }
                }

                sb.append("\n");
            }
        }

        return sb.toString();
    }
}

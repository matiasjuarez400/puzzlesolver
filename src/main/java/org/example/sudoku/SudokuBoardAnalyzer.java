package org.example.sudoku;

import java.util.Arrays;

public class SudokuBoardAnalyzer {
    private final SudokuBoard sudokuBoard;
    private final int[] workingArray;

    public SudokuBoardAnalyzer(SudokuBoard sudokuBoard) {
        this.sudokuBoard = sudokuBoard;
        this.workingArray = new int[sudokuBoard.getBoardLength() + 1];
    }

    public boolean isValidMove(final int row, final int col, final int value) {
        if (sudokuBoard.isReadOnly(row, col)) return false;

        if (isValuePresentInRow(row, value)) return false;

        if (isValuePresentInColumn(col, value)) return false;

        if (isValuePresentInArea(row, col, value)) return false;

        return true;
    }

    public boolean isValidBoard() {
        for (int row = 0; row < sudokuBoard.getBoardLength(); row++) {
            if (!isValidRow(row)) return false;
        }

        for (int col = 0; col < sudokuBoard.getBoardLength(); col++) {
            if (!isValidColumn(col)) return false;
        }

        for (int areaRow = 0; areaRow < sudokuBoard.getAreasInLine(); areaRow++) {
            for (int areaCol = 0; areaCol < sudokuBoard.getAreasInLine(); areaCol++) {
                if (!isValidArea(areaRow, areaCol)) return false;
            }
        }

        return true;
    }

    public boolean isSolvedBoard() {
        for (int row = 0; row < sudokuBoard.getBoardLength(); row++) {
            for (int col = 0; col < sudokuBoard.getBoardLength(); col++) {
                if (sudokuBoard.getValue(row, col) == 0) return false;
            }
        }

        return isValidBoard();
    }

    private boolean isValidRow(final int row) {
        Arrays.fill(workingArray, 0);

        for (int col = 0; col < sudokuBoard.getBoardLength(); col++) {
            if (!checkIfValueIsValid(row, col)) {
                return false;
            }
        }

        return true;
    }

    private boolean isValuePresentInRow(final int row, final int value) {
        for (int col = 0; col < sudokuBoard.getBoardLength(); col++) {
            if (sudokuBoard.getValue(row, col) == value) return true;
        }

        return false;
    }

    private boolean isValidColumn(final int col) {
        Arrays.fill(workingArray, 0);

        for (int row = 0; row < sudokuBoard.getBoardLength(); row++) {
            if (!checkIfValueIsValid(row, col)) {
                return false;
            }
        }

        return true;
    }

    private boolean isValuePresentInColumn(final int col, final int value) {
        for (int row = 0; row < sudokuBoard.getBoardLength(); row++) {
            if (sudokuBoard.getValue(row, col) == value) return true;
        }

        return false;
    }

    private boolean isValidArea(final int areaRow, final int areaCol) {
        Arrays.fill(workingArray, 0);

        final int areaSize = sudokuBoard.getAreaLength();

        for (int row = 0; row < areaSize; row++) {
            for (int col = 0; col < areaSize; col++) {
                if (!checkIfValueIsValid(areaRow * areaSize + row, areaCol * areaSize + col)) {
                    return false;
                }
            }
        }

        return true;
    }

    private boolean isValuePresentInArea(final int row, final int col, final int value) {
        final int areaRow = translateToAreaIndex(row);
        final int areaCol = translateToAreaIndex(col);
        final int areaSize = sudokuBoard.getAreaLength();

        for (int r = 0; r < areaSize; r++) {
            for (int c = 0; c < areaSize; c++) {
                final int currentValue = sudokuBoard.getValue(areaRow * areaSize + r, areaCol * areaSize + c);
                if (currentValue == value) return true;
            }
        }

        return false;
    }

    private int translateToAreaIndex(final int boardIndex) {
        return boardIndex / sudokuBoard.getAreaLength();
    }

    private boolean checkIfValueIsValid(final int row, final int col) {
        final int currentValue = sudokuBoard.getValue(row, col);

        if (workingArray[currentValue] != 0) {
            return false;
        }

        workingArray[currentValue] = currentValue;

        return true;
    }
}

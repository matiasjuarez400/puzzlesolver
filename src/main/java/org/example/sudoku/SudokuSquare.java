package org.example.sudoku;

import lombok.Getter;

@Getter
public class SudokuSquare {
    private int value;
    private final boolean readOnly;

    public SudokuSquare(int value, boolean readOnly) {
        this.value = value;
        this.readOnly = readOnly;
    }

    public SudokuSquare(int value) {
        this(value, false);
    }

    public void setValue(final int value) {
        if (readOnly) {
            throw new IllegalStateException("This square is readOnly");
        }

        this.value = value;
    }
}

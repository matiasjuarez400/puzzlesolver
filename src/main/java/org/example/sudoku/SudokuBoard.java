package org.example.sudoku;

import lombok.Getter;

public class SudokuBoard {
    private final SudokuSquare[][] board;
    @Getter
    private final int boardLength;
    @Getter
    private final int areaLength;
    @Getter
    private final int areasInLine;

    public SudokuBoard(final int[][] board, int areaLength) {
        if (board.length != board[0].length) {
            throw new IllegalArgumentException("SudokuBoard must be a square");
        }

        if (board.length % areaLength != 0) {
            throw new IllegalArgumentException("A whole number of areas must fit inside the board");
        }

        if (areaLength * areaLength != board.length) {
            throw new IllegalArgumentException("The number of elements inside an area must be the same as the length of the board");
        }

        checkBoardAndAreaSize(board.length, board[0].length, areaLength);

        this.areaLength = areaLength;
        this.boardLength = board.length;
        this.areasInLine = boardLength / areaLength;

        this.board = new SudokuSquare[board.length][board[0].length];

        for (int i = 0; i < boardLength; i++) {
            for (int j = 0; j < boardLength; j++) {
                final int value = Math.abs(board[i][j]);
                final SudokuSquare sudokuSquare = new SudokuSquare(value, value < 0);
                this.board[i][j] = sudokuSquare;
            }
        }
    }

    public int getValue(final int row, final int col) {
        return this.board[row][col].getValue();
    }

    public boolean isReadOnly(final int row, final int col) {
        return this.board[row][col].isReadOnly();
    }

    public void setValue(final int row, final int col, final int value) {
        if (isReadOnly(row, col)) {
            throw new IllegalStateException(String.format("The square is readOnly [%d, %d]", row, col));
        }
        this.board[row][col].setValue(value);
    }

    private void checkBoardAndAreaSize(final int boardRows, final int boardCols, final int areaSize) {
        if (boardRows != boardCols) {
            throw new IllegalArgumentException("SudokuBoard must be a square");
        }

        if (boardRows % areaSize != 0) {
            throw new IllegalArgumentException("A whole number of areas must fit inside the board");
        }
    }
}

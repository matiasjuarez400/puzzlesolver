package org.example.nonogram.solver.filler;

import lombok.Getter;
import org.example.nonogram.board.NonogramBoard;
import org.example.nonogram.board.SquareStatus;

import java.util.ArrayList;
import java.util.List;

@Getter
public class BoardElementsAnalysis {
    private final List<Group> filledGroups;
    private final List<Group> emptyGroups;
    private final List<Group> blockedGroups;

    public BoardElementsAnalysis(final NonogramBoard.BoardElements boardElements) {
        this.filledGroups = new ArrayList<>();
        this.emptyGroups = new ArrayList<>();
        this.blockedGroups = new ArrayList<>();

        analyzeElements(boardElements);
    }

    private void analyzeElements(final NonogramBoard.BoardElements boardElements) {
        SquareStatus currentStatus = boardElements.get(0);
        int currentStart = 0;
        int currentEnd = 0;

        for (int i = 1; i < boardElements.length(); i++) {
            final SquareStatus nextStatus = boardElements.get(i);

            if (nextStatus.equals(currentStatus)) {
                currentEnd++;
            } else {
                final Group newGroup = new Group(currentStatus, currentStart, currentEnd);

                if (currentStatus.isBlocked()) {
                    blockedGroups.add(newGroup);
                } else if (currentStatus.isFilled()) {
                    filledGroups.add(newGroup);
                } else {
                    emptyGroups.add(newGroup);
                }

                currentStatus = nextStatus;
                currentStart = i;
                currentEnd = i;
            }
        }
    }
}

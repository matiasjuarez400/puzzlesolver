package org.example.nonogram.solver.filler;

import lombok.Getter;
import org.example.nonogram.board.SquareStatus;

@Getter
public class Group {
    private final SquareStatus groupStatus;
    private final int beginIndex;
    private final int endIndex;

    public Group(SquareStatus groupStatus, int beginIndex, int endIndex) {
        this.groupStatus = groupStatus;
        this.beginIndex = beginIndex;
        this.endIndex = endIndex;
    }

    public int size() {
        return endIndex - beginIndex + 1;
    }
}

package org.example.nonogram.solver.filler;

import org.example.nonogram.board.NonogramBoard;
import org.example.nonogram.solver.board_elements.BoardElementsRegionAnalysis;
import org.example.nonogram.solver.board_elements.BoardElementsUncertainRegions;
import org.example.nonogram.solver.board_elements.Region;
import org.example.nonogram.solver.board_elements.UncertainRegion;
import org.example.nonogram.solver.board_elements.UncertaintyAnalyzer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Filler {
    private final UncertaintyAnalyzer uncertaintyAnalyzer;

    public Filler() {
        this.uncertaintyAnalyzer = new UncertaintyAnalyzer();
    }

    public void fill(final NonogramBoard nonogramBoard) {
        int currentFilledSquares = nonogramBoard.countFilledSquares();

        boolean stuck = false;

        while (!stuck) {
            runOneDimensionAlgorithms(nonogramBoard);

            int newCurrentCount = nonogramBoard.countFilledSquares();

            if (newCurrentCount == currentFilledSquares) {
                stuck = true;
            } else {
                currentFilledSquares = newCurrentCount;
            }
        }
    }

    private void runOneDimensionAlgorithms(final NonogramBoard nonogramBoard) {
        final List<NonogramBoard.BoardElements> boardElementsList = nonogramBoard.getAllBoardElements();

        for (NonogramBoard.BoardElements boardElements : boardElementsList) {
//            fillDisconnectedIslands(boardElements);
//            fillGroupsThatStemFromBorder(boardElements);
//            fillGroupsThatStemFromBlockedSquare(boardElements);
            runUncertainRegionsAlgorithms(boardElements);
        }
    }

    private void runUncertainRegionsAlgorithms(final NonogramBoard.BoardElements boardElements) {
        final BoardElementsUncertainRegions boardElementsUncertainRegions = uncertaintyAnalyzer.calculateUncertainRegions(boardElements);

        fillBasedOnUncertainRegionSelfOverlapping(boardElements, boardElementsUncertainRegions);
        fillUncertainRegionsCollapsingIntoOne(boardElements, boardElementsUncertainRegions);
        fillBasedOnSquaresBelongingToOneUncertainRegion(boardElements, boardElementsUncertainRegions);
        fillByDiscardingRegionsWithAdjacentFilledSquares(boardElements, boardElementsUncertainRegions);
    }

    private void fillByDiscardingRegionsWithAdjacentFilledSquares(final NonogramBoard.BoardElements boardElements,
                                             final BoardElementsUncertainRegions uncertainRegions) {
        for (int restrictionIndex = 0; restrictionIndex < boardElements.getRestrictions().length; restrictionIndex++) {
            final UncertainRegion uncertainRegion = uncertainRegions.getUncertainRegion(restrictionIndex);

            final List<Region> validRegions = new ArrayList<>(uncertainRegion.getAllPossibleRegions());

            validRegions.removeIf(nextRegion -> regionHasAdjacentFilledSquares(nextRegion, boardElements));

            if (validRegions.size() == 1) {
                final Region certainRegion = validRegions.get(0);
                for (int i = certainRegion.getStart(); i <= certainRegion.getEnd(); i++) {
                    boardElements.fill(i);
                }
            }
        }
    }

    private boolean regionHasAdjacentFilledSquares(final Region region, final NonogramBoard.BoardElements elements) {
        if (region.getStart() > 0) {
            if (elements.get(region.getStart() - 1).isFilled()) {
                return true;
            }
        }

        if ((region.getEnd() + 1) < elements.length()) {
            if (elements.get(region.getEnd() + 1).isFilled()) {
                return true;
            }
        }

        return false;
    }

    private int countRegionAdjacentFilledSquares(final Region region, final NonogramBoard.BoardElements elements) {
        int counter = 0;

        counter += countRegionAdjacentFilledSquares(region.getStart() - 1, -1, elements);
        counter += countRegionAdjacentFilledSquares(region.getEnd() + 1, 1, elements);

        return counter;
    }

    private int countRegionAdjacentFilledSquares(final int startIndex, final int direction, final NonogramBoard.BoardElements elements) {
        int counter = 0;
        for (int i = startIndex; i >= 0 && i < elements.length(); i += direction) {
            if (elements.get(i).isFilled()) {
                counter++;
            } else {
                break;
            }
        }

        return counter;
    }

    private void fillBasedOnSquaresBelongingToOneUncertainRegion(final NonogramBoard.BoardElements boardElements,
                                                                 final BoardElementsUncertainRegions boardElementsUncertainRegions) {
        final int BLOCKED_REGION = -2;
        final int UNKNOWN_REGION = -1;

        final int[] belongingRegion = new int[boardElements.length()];
        Arrays.fill(belongingRegion, UNKNOWN_REGION);

        for (int restrictionIndex = 0; restrictionIndex < boardElements.getRestrictions().length; restrictionIndex++) {
            final UncertainRegion uncertainRegion = boardElementsUncertainRegions.getUncertainRegion(restrictionIndex);

            for (int squareIndex = 0; squareIndex < belongingRegion.length; squareIndex++) {
                final int squareValue = belongingRegion[squareIndex];

                if (squareValue == BLOCKED_REGION) continue;

                if (uncertainRegion.contains(squareIndex)) {
                    if (squareValue == UNKNOWN_REGION || squareValue == restrictionIndex) {
                        belongingRegion[squareIndex] = restrictionIndex;
                    } else {
                        belongingRegion[squareIndex] = BLOCKED_REGION;
                    }
                }
            }
        }

        final int[] distinctRegions = Arrays.stream(belongingRegion)
                .filter(i -> i >= 0)
                .distinct()
                .toArray();

        final List<Region> exclusiveRegions = new ArrayList<>();
        for (final int distinctRegion : distinctRegions) {
            int startRegion = -1;
            int endRegion = -1;

            for (int i = 0; i < belongingRegion.length; i++) {
                final int nextRegion = belongingRegion[i];

                if (nextRegion == distinctRegion) {
                    if (startRegion == -1) {
                        startRegion = i;
                    }
                    endRegion = i;
                }
            }

            final Region exclusiveRegion = new Region(startRegion, endRegion);
            exclusiveRegions.add(exclusiveRegion);
        }

        for (final Region region : exclusiveRegions) {
            int leftMostFilledSquare = -1;
            int rightMostFilledSquare = -1;

            for (int i = region.getStart(); i <= region.getEnd(); i++) {
                if (boardElements.get(i).isFilled()) {
                    if (leftMostFilledSquare == -1) {
                        leftMostFilledSquare = i;
                    }
                    rightMostFilledSquare = i;
                }
            }

            if (leftMostFilledSquare == -1) continue;

            for (int i = leftMostFilledSquare; i <= rightMostFilledSquare; i++) {
                if (boardElements.get(i).isBlocked()) {
                    throw new IllegalStateException("There should not be a blocking here");
                }

                boardElements.fill(i);
            }
        }
    }

    private void fillBasedOnUncertainRegionSelfOverlapping(final NonogramBoard.BoardElements boardElements,
                                                           final BoardElementsUncertainRegions boardElementsUncertainRegions) {
        for (int restrictionIndex = 0; restrictionIndex < boardElements.getRestrictions().length; restrictionIndex++) {
            final UncertainRegion uncertainRegion = boardElementsUncertainRegions.getUncertainRegion(restrictionIndex);

            final List<Region> regions = uncertainRegion.getAllPossibleRegions();

            fillSquaresBasedOnOverlappingRegions(boardElements, regions);
        }
    }

    private void fillSquaresBasedOnOverlappingRegions(final NonogramBoard.BoardElements boardElements, final List<Region> regions) {
        final Map<Integer, Integer> counterByRegionIndex = new HashMap<>();

        for (final Region region : regions) {
            for (int regionIndex = region.getStart(); regionIndex <= region.getEnd(); regionIndex++) {
                counterByRegionIndex.compute(regionIndex, (k, v) -> v == null ? 1 : v + 1);
            }
        }

        for (final Integer regionIndex : counterByRegionIndex.keySet()) {
            final Integer counter = counterByRegionIndex.get(regionIndex);

            if (counter.equals(regions.size())) {
                if (boardElements.get(regionIndex).isBlocked()) {
                    throw new IllegalStateException("This square should not be blocked");
                }

                boardElements.fill(regionIndex);
            }
        }
    }

    private void fillUncertainRegionsCollapsingIntoOne(final NonogramBoard.BoardElements boardElements,
                                                       final BoardElementsUncertainRegions boardElementsUncertainRegions) {
        for (int restrictionIndex = 0; restrictionIndex < boardElements.getRestrictions().length; restrictionIndex++) {
            final UncertainRegion restrictionUncertainRegion = boardElementsUncertainRegions.getUncertainRegion(restrictionIndex);

            final List<Region> allRestrictionPossibleRegions = restrictionUncertainRegion.getAllPossibleRegions();

            final List<Region> regionsThatFit = new ArrayList<>();

            for (final Region possibleRegion : allRestrictionPossibleRegions) {
                boolean regionFits = true;

                for (int regionIndex = possibleRegion.getStart(); regionIndex <= possibleRegion.getEnd(); regionIndex++) {
                    if (boardElements.get(regionIndex).isBlocked()) {
                        regionFits = false;
                        break;
                    }
                }

                if (regionFits) {
                    regionsThatFit.add(possibleRegion);
                }
            }

            if (regionsThatFit.size() == 1) {
                final Region certainRegion = regionsThatFit.get(0);

                for (int regionIndex = certainRegion.getStart(); regionIndex <= certainRegion.getEnd(); regionIndex++) {
                    boardElements.fill(regionIndex);
                }
            }
        }
    }

    private void fillDisconnectedIslands(final NonogramBoard.BoardElements boardElements) {
        final BoardElementsRegionAnalysis boardElementsAnalysis = new BoardElementsRegionAnalysis(boardElements);

        if (boardElements.getRestrictions().length == 1) {
            if (boardElementsAnalysis.getFilledRegions().size() > 1) {
                for (int i = 1; i < boardElementsAnalysis.getFilledRegions().size(); i++) {
                    final Region leftRegion = boardElementsAnalysis.getFilledRegions().get(i - 1);
                    final Region rightRegion = boardElementsAnalysis.getFilledRegions().get(i);

                    for (int j = leftRegion.getEnd() + 1; j < rightRegion.getStart(); j++) {
                        if (boardElements.get(j).isBlocked()) {
                            throw new IllegalStateException("There should not be anything here");
                        }

                        boardElements.fill(j);
                    }
                }
            }
        }
    }

    private void fillGroupsThatStemFromBorder(final NonogramBoard.BoardElements boardElements) {
        final BoardElementsAnalysis analysis = new BoardElementsAnalysis(boardElements);

        if (analysis.getFilledGroups().isEmpty()) return;

        final Group firstGroup = analysis.getFilledGroups().get(0);
        if (firstGroup.getBeginIndex() == 0) {
            final int firstRestriction = boardElements.getRestrictions()[0];

            fillGroupInDirection(firstGroup, 1, firstRestriction, boardElements);
        }


        final Group lastGroup = analysis.getFilledGroups().get(analysis.getFilledGroups().size() - 1);

        if (!lastGroup.equals(firstGroup) && boardElements.getRestrictions().length > 1) {
            if (lastGroup.getEndIndex() == boardElements.length() - 1) {
                final int lastRestriction = boardElements.getRestrictions()[boardElements.getRestrictions().length - 1];

                fillGroupInDirection(lastGroup, -1, lastRestriction, boardElements);
            }
        }
    }

    private void fillGroupsThatStemFromBlockedSquare(final NonogramBoard.BoardElements boardElements) {
        final BoardElementsAnalysis analysis = new BoardElementsAnalysis(boardElements);

        for (final Group blockGroup : analysis.getBlockedGroups()) {
            final int leftSide = blockGroup.getBeginIndex() - 1;

            if (leftSide > 0 && boardElements.get(leftSide).isFilled()) {

            }
        }
    }

    private int checkMaxGroupSize(final int beginIndex, final int direction, final NonogramBoard.BoardElements boardElements) {
        int maxGroupSize = 0;

        for (int i = beginIndex; i >= 0 && i < boardElements.length(); i += direction) {
            if (boardElements.get(i).isFilled() || boardElements.get(i).isEmpty()) {
                maxGroupSize++;
            } else {
                break;
            }
        }

        return maxGroupSize;
    }

    private void fillGroupInDirection(final Group group,
                                      final int direction,
                                      final int restriction,
                                      final NonogramBoard.BoardElements boardElements) {
        if (group.size() < restriction) {
            final int startIndex = direction == 1 ? group.getEndIndex() + 1 : group.getBeginIndex() - 1;
            final int missingSquares = restriction - group.size();

            for (int i = 0; i < missingSquares; i++) {
                final int nextIndex = startIndex + (i * direction);

                if (!boardElements.get(nextIndex).isEmpty()) {
                    throw new IllegalStateException("There should not be anything here");
                }

                boardElements.fill(nextIndex);
            }
        }
    }
}

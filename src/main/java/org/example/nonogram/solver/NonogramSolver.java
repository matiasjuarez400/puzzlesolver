package org.example.nonogram.solver;

import org.example.nonogram.board.NonogramBoard;
import org.example.nonogram.solver.filler.Filler;

public class NonogramSolver {
    private final Blocker blocker;
    private final Filler filler;

    public NonogramSolver() {
        this.blocker = new Blocker();
        this.filler = new Filler();
    }

    public boolean solve(final NonogramBoard nonogramBoard) {
        int emptySquares = nonogramBoard.countEmptySquares();

        boolean stuck = false;

        while (!stuck) {
            blocker.blockSquares(nonogramBoard);
            filler.fill(nonogramBoard);

            final int currentEmptySquares = nonogramBoard.countEmptySquares();

            if (emptySquares == currentEmptySquares) {
                stuck = true;
            } else {
                emptySquares = currentEmptySquares;
            }
        }

        return nonogramBoard.isSolved();
    }
}

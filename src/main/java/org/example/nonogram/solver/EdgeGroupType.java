package org.example.nonogram.solver;

public enum  EdgeGroupType {
    LEFT, RIGHT, BOTH, NONE;

    public boolean isLeft() {
        return LEFT.equals(this);
    }

    public boolean isRight() {
        return RIGHT.equals(this);
    }

    public boolean isBoth() {
        return BOTH.equals(this);
    }

    public boolean isEdgeGroup() {
        return !NONE.equals(this);
    }
}

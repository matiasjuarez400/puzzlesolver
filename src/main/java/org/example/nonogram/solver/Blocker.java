package org.example.nonogram.solver;

import org.example.nonogram.board.NonogramBoard;

import java.util.Arrays;
import java.util.List;

public class Blocker {
    public void blockSquares(final NonogramBoard nonogramBoard) {
        int blockedSquares = nonogramBoard.countBlockedSquares();
        boolean countSquaresChanged = true;

        while (countSquaresChanged) {
            runBlockingAlgorithms(nonogramBoard);

            int currentBlockedSquares = nonogramBoard.countBlockedSquares();

            if (currentBlockedSquares == blockedSquares) {
                countSquaresChanged = false;
            }

            blockedSquares = currentBlockedSquares;
        }

    }

    private void runBlockingAlgorithms(final NonogramBoard nonogramBoard) {
        final List<NonogramBoard.BoardElements> boardElementsList = nonogramBoard.getAllBoardElements();

        for (final NonogramBoard.BoardElements elements : boardElementsList) {
            blockByFullBoardElementRestrictions(elements);
        }
    }

    private void blockByFullBoardElementRestrictions(final NonogramBoard.BoardElements elements) {
        final int totalRestrictions = Arrays.stream(elements.getRestrictions()).sum();

        int filledSquaresCount = 0;
        for (int i = 0; i < elements.length(); i++) {
            if (elements.get(i).isFilled()) {
                filledSquaresCount++;
            }
        }

        if (filledSquaresCount > totalRestrictions) {
            throw new IllegalStateException("The total number of filled squares is superior to the total restrictions");
        } else if (filledSquaresCount == totalRestrictions) {
            for (int i = 0; i < elements.length(); i++) {
                if (elements.isEmpty(i)) {
                    elements.block(i);
                }
            }
        }
    }

    private void runBlockingStrategies(final NonogramBoard.BoardElements boardElements, final int[] restrictions) {
        blockCompletedRestrictions(boardElements, restrictions);
        blockUnreachableSquaresByDistance(boardElements, restrictions);
        blockUnreachableSquaresByBlockedSquares(boardElements, restrictions);
    }

    private void blockUnreachableSquaresByBlockedSquares(final NonogramBoard.BoardElements boardElements, final int[] restrictions) {
        if (restrictions.length == 1) {
            int groupStart = -1;
            int groupEnd = -1;

            for (int i = 0; i < boardElements.length(); i++) {
                if (boardElements.get(i).isFilled()) {
                    if (groupStart < 0) {
                        groupStart = i;
                    }

                    groupEnd = i;
                }
            }

            boolean startLeftBlocking = false;
            for (int i = groupStart - 1; i >= 0; i--) {
                if (startLeftBlocking) {
                    blockIfValid(i, boardElements);
                    continue;
                }

                if (boardElements.get(i).isBlocked()) {
                    startLeftBlocking = true;
                }
            }

            boolean startRightBlocking = false;
            for (int i = groupEnd + 1; i < boardElements.length(); i++) {
                if (startRightBlocking) {
                    blockIfValid(i, boardElements);
                    continue;
                }

                if (boardElements.get(i).isBlocked()) {
                    startRightBlocking = true;
                }
            }
        }
    }

    private void blockUnreachableSquaresByDistance(final NonogramBoard.BoardElements boardElements, final int[] restrictions) {
        if (restrictions.length == 1) {
            int groupStart = -1;
            int groupEnd = -1;

            for (int i = 0; i < boardElements.length(); i++) {
                if (boardElements.get(i).isFilled()) {
                    if (groupStart < 0) {
                        groupStart = i;
                    }

                    groupEnd = i;
                }
            }

            final int currentGroupSize = groupEnd - groupStart + 1;
            final int missingSquaresForCompletion = restrictions[0] - currentGroupSize;

            final int leftEndBlocking = groupStart - missingSquaresForCompletion;
            final int rightStartBlocking = groupEnd + 1 + missingSquaresForCompletion;

            for (int i = 0; i < leftEndBlocking; i++) {
                blockIfValid(i, boardElements);
            }

            for (int i = rightStartBlocking; i < boardElements.length(); i++) {
                blockIfValid(i, boardElements);
            }
        }
    }

    private void blockCompletedRestrictions(final NonogramBoard.BoardElements elements,
                                                   final int[] restrictions) {
        final int maxRestrictionSize = Arrays.stream(restrictions).max().orElseThrow(() -> new RuntimeException("Empty restrictions?"));

        int groupSize = 0;
        int groupStart = -1;

        for (int i = 0; i < elements.length(); i++) {
            if (elements.get(i).isFilled()) {
                if (groupSize == 0) {
                    groupStart = i;
                }
                groupSize++;
            } else {
                blockGroupIfMaxRestrictionSize(groupSize, groupStart, maxRestrictionSize, elements);
                blockIfGroupIsCompleteAndIsExtreme(groupSize, groupStart, restrictions, elements);
                groupSize = 0;
            }
        }

        blockGroupIfMaxRestrictionSize(groupSize, groupStart, maxRestrictionSize, elements);
        blockIfGroupIsCompleteAndIsExtreme(groupSize, groupStart, restrictions, elements);
    }

    private void blockIfGroupIsCompleteAndIsExtreme(final int groupSize,
                                                    final int groupStart,
                                                    final int[] restrictions,
                                                    final NonogramBoard.BoardElements elements) {
        if (groupSize == 0) {
            return;
        }

        final EdgeGroupType edgeGroupType = calculateEdgeGroupType(groupSize, groupStart, restrictions, elements.length());

        if (edgeGroupType.isLeft() || edgeGroupType.isBoth()) {
            for (int i = 0; i < groupStart; i++) {
                if (elements.get(i).isFilled()) {
                    throw new IllegalStateException("Error in blocking logic: The group is an edge group, so no filled blocks should be here or the group is not edge");
                }
                elements.block(i);
            }
        }

        if (edgeGroupType.isRight() || edgeGroupType.isBoth()) {
            for (int i = groupStart + groupSize; i < elements.length(); i++) {
                if (elements.get(i).isFilled()) {
                    throw new IllegalStateException("Error in blocking logic: The group is an edge group, so no filled blocks should be here or the group is not edge");
                }
                elements.block(i);
            }
        }
    }

    private void blockGroupIfMaxRestrictionSize(final int groupSize,
                                                final int groupStart,
                                                final int maxRestrictionSize,
                                                final NonogramBoard.BoardElements elements) {
        if (groupStart < 0) return;

        final int leftIndex = groupStart - 1;
        final int rightIndex = groupStart + groupSize;

        if (groupSize == maxRestrictionSize) {
            blockIfValid(rightIndex, elements);
            blockIfValid(leftIndex, elements);
        }
    }

    private EdgeGroupType calculateEdgeGroupType(final int groupSize,
                                                 final int groupStart,
                                                 final int[] restrictions,
                                                 final int elementCount) {
        final int fittingGroupThreshold = groupSize + 1;

        boolean isLeft = false;
        boolean isRight = false;

        if (restrictions[0] == groupSize) {
            int squaresFromStart = groupStart + 1;

            isLeft = squaresFromStart < fittingGroupThreshold;
        }

        if (restrictions[restrictions.length - 1] == groupSize) {
            int squaresAtTheEnd = elementCount - (groupStart + groupSize);

            isRight = squaresAtTheEnd < fittingGroupThreshold;
        }

        if (isRight && isLeft) {
            return EdgeGroupType.BOTH;
        } else if (isRight) {
            return EdgeGroupType.RIGHT;
        } else if (isLeft) {
            return EdgeGroupType.LEFT;
        } else {
            return EdgeGroupType.NONE;
        }
    }

    private void blockIfValid(final int index, final NonogramBoard.BoardElements elements) {
        if (index < 0 || index >= elements.length()) {
            return;
        }

        if (elements.get(index).isFilled()) {
            throw new IllegalArgumentException("Trying to block filled square!");
        }

        elements.block(index);
    }
}

package org.example.nonogram.solver.board_elements;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class UncertainRegion {
    private final int uncertaintyLevel;
    private final Region baseRegion;

    public UncertainRegion(int uncertaintyLevel, Region baseRegion) {
        this.uncertaintyLevel = uncertaintyLevel;
        this.baseRegion = baseRegion;
    }

    public UncertainRegion(int uncertaintyLevel, int baseRegionStart, int baseRegionEnd) {
        this(uncertaintyLevel, new Region(baseRegionStart, baseRegionEnd));
    }

    public List<Region> getAllPossibleRegions() {
        final List<Region> regions = new ArrayList<>();

        for (int i = 0; i <= uncertaintyLevel; i++) {
            regions.add(new Region(baseRegion.getStart() + i, baseRegion.getEnd() + i));
        }

        return regions;
    }

    public int size() {
        return baseRegion.size() + uncertaintyLevel;
    }

    public int getStartIndex() {
        return baseRegion.getStart();
    }

    public int getEndIndex() {
        return getStartIndex() + size() - 1;
    }

    public boolean contains(final int index) {
        return getStartIndex() <= index && index <= getEndIndex();
    }
}

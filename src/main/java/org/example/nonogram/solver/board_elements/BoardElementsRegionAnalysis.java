package org.example.nonogram.solver.board_elements;

import lombok.Getter;
import org.example.nonogram.board.NonogramBoard;
import org.example.nonogram.board.SquareStatus;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

@Getter
public class BoardElementsRegionAnalysis {
    private final List<Region> blockedRegions;
    private final List<Region> emptyRegions;
    private final List<Region> filledRegions;
    private final List<Region> fillableRegions;
    private final NonogramBoard.BoardElements elements;

    public BoardElementsRegionAnalysis(final NonogramBoard.BoardElements boardElements) {
        this.blockedRegions = getRegions(boardElements, EnumSet.of(SquareStatus.BLOCKED));
        this.emptyRegions = getRegions(boardElements, EnumSet.of(SquareStatus.EMPTY));
        this.filledRegions = getRegions(boardElements, EnumSet.of(SquareStatus.FILLED));
        this.fillableRegions = getRegions(boardElements, EnumSet.of(SquareStatus.FILLED, SquareStatus.EMPTY));
        this.elements = boardElements;
    }

    private List<Region> getRegions(final NonogramBoard.BoardElements elements, final Set<SquareStatus> requiredStatuses) {
        int regionStart = -1;
        int regionEnd = -1;

        final List<Region> regions = new ArrayList<>();

        for (int i = 0; i < elements.length(); i++) {
            if (requiredStatuses.contains(elements.get(i))) {
                if (regionStart < 0) {
                    regionStart = i;
                }

                regionEnd = i;
            } else {
                if (regionStart >= 0) {
                    final Region region = new Region(regionStart, regionEnd);
                    regions.add(region);
                }

                regionStart = -1;
                regionEnd = -1;
            }
        }

        return regions;
    }
}

package org.example.nonogram.solver.board_elements;

import org.example.nonogram.board.NonogramBoard;

public class UncertaintyAnalyzer {
    public BoardElementsUncertainRegions calculateUncertainRegions(final NonogramBoard.BoardElements elements) {
        final int uncertaintyLevel = calculateUncertainty(elements.getRestrictions(), elements.length());

        final BoardElementsUncertainRegions boardElementsUncertainRegions = new BoardElementsUncertainRegions(elements);

        for (int restrictionIndex = 0; restrictionIndex < elements.getRestrictions().length; restrictionIndex++) {
            final int restriction = elements.getRestrictions()[restrictionIndex];

            final UncertainRegion lastUncertainRegion = boardElementsUncertainRegions.getLastUncertainRegion();
            final int baseRegionStartIndex = lastUncertainRegion == null ? 0 : lastUncertainRegion.getBaseRegion().getEnd() + 2;
            final int baseRegionEndIndex = baseRegionStartIndex + restriction - 1;

            final UncertainRegion nextUncertainRegion = new UncertainRegion(uncertaintyLevel, baseRegionStartIndex, baseRegionEndIndex);

            boardElementsUncertainRegions.addUncertainRegion(nextUncertainRegion, restrictionIndex);
        }

        return boardElementsUncertainRegions;
    }

    private int calculateUncertainty(final int[] restrictions, final int squareCount) {
        int squaresWithUncertainty = squareCount;

        for (int i = 0; i < restrictions.length - 1; i++) {
            squaresWithUncertainty -= (restrictions[i] + 1);
        }

        squaresWithUncertainty -= restrictions[restrictions.length - 1];

        return squaresWithUncertainty;
    }
}

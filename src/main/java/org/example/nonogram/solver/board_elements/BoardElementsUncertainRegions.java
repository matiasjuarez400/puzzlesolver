package org.example.nonogram.solver.board_elements;

import lombok.Getter;
import org.example.nonogram.board.NonogramBoard;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BoardElementsUncertainRegions {
    @Getter
    private final NonogramBoard.BoardElements elements;
    private final Map<Integer, UncertainRegion> uncertainRegionMap;

    public BoardElementsUncertainRegions(final NonogramBoard.BoardElements elements) {
        this.elements = elements;
        this.uncertainRegionMap = new HashMap<>();
    }

    public void addUncertainRegion(final UncertainRegion uncertainRegion, final int restrictionIndex) {
        uncertainRegionMap.put(restrictionIndex, uncertainRegion);
    }

    public UncertainRegion getUncertainRegion(final Integer restrictionIndex) {
        return uncertainRegionMap.get(restrictionIndex);
    }

    public UncertainRegion getLastUncertainRegion() {
        final Integer maxRestrictionIndex = uncertainRegionMap.keySet().stream()
                .max(Comparator.naturalOrder())
                .orElse(-1);

        return maxRestrictionIndex >= 0 ? getUncertainRegion(maxRestrictionIndex) : null;
    }

    public List<UncertainRegion> getUncertainRegions() {
        return new ArrayList<>(uncertainRegionMap.values());
    }
}

package org.example.nonogram.solver.board_elements;

import lombok.Getter;

@Getter
public class Region {
    private final int start;
    private final int end;

    public Region(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public int size() {
        return end - start + 1;
    }
}

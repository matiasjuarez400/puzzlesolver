package org.example.nonogram;

import org.example.nonogram.board.NonogramBoard;
import org.example.nonogram.solver.NonogramSolver;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Application {
    public static void main(String[] args) throws IOException {
        final NonogramLoader nonogramLoader = new NonogramLoader();

        final Path file = Paths.get("D:\\Documents\\_RealDocs\\Programming\\generalpuzzlesolver\\src\\main\\resources\\nonogram\\example1.json");

        final NonogramBoard nonogramBoard = nonogramLoader.load(file);

        final NonogramSolver nonogramSolver = new NonogramSolver();
        final boolean isSolved = nonogramSolver.solve(nonogramBoard);

        final NonogramPrinter nonogramPrinter = new NonogramPrinter();

        nonogramPrinter.print(nonogramBoard);

        System.out.println("Solved: " + isSolved);
    }
}

package org.example.nonogram;

import org.example.nonogram.board.NonogramBoard;

import java.util.ArrayList;
import java.util.List;

public class NonogramPrinter {
    private static final String BLOCK = "X";
    private static final String FILLED = "o";
    private static final String EMPTY = "-";

    public String toStringRepresentation(final NonogramBoard nonogramBoard) {
        final int longestRowRestriction = getLongestRowRestriction(nonogramBoard);
        final int longestColRestriction = getLongestColRestriction(nonogramBoard);

        final String colRestrictionSection = buildColRestrictionSection(nonogramBoard, longestRowRestriction, longestColRestriction);
        final String rows = buildRows(longestRowRestriction, nonogramBoard);

        return String.join("\n", colRestrictionSection, rows);
    }

    private String buildColRestrictionSection(final NonogramBoard nonogramBoard,
                                       final int longestRowRestriction,
                                       final int longestColRestriction) {
        final int[][] colRestrictions = new int[nonogramBoard.getCols()][];

        for (int col = 0; col < nonogramBoard.getCols(); col++) {
            colRestrictions[col] = nonogramBoard.getColRestriction(col);
        }

        return buildColRestrictionSection(longestRowRestriction, longestColRestriction, colRestrictions);
    }

    private String buildColRestrictionSection(final int longestRowRestriction, final int longestColRestriction, final int[][] colRestrictions) {
        final String leftPadding = buildLeftPadding(longestRowRestriction);
        final String[][] colRestrictionsWithPadding = buildColRestrictionsWithPadding(longestColRestriction, colRestrictions);

        final List<String> restrictionRows = new ArrayList<>();

        for (int restrictionRow = 0; restrictionRow < longestColRestriction; restrictionRow++) {
            final List<String> restrictionRowElements = new ArrayList<>();
            restrictionRowElements.add(leftPadding);


            for (int restrictionCol = 0; restrictionCol < colRestrictionsWithPadding.length; restrictionCol++) {
                restrictionRowElements.add(colRestrictionsWithPadding[restrictionCol][restrictionRow]);
            }

            final String rowString = String.join(" ", restrictionRowElements);

            restrictionRows.add(rowString);
        }

        return String.join("\n", restrictionRows);
    }

    private String[][] buildColRestrictionsWithPadding(final int longestColRestriction, final int[][] colRestrictions) {
        final String[][] output = new String[colRestrictions.length][longestColRestriction];

        for (int col = 0; col < colRestrictions.length; col++) {
            final int upPadding = longestColRestriction - colRestrictions[col].length;

            for (int i = 0; i < upPadding; i++) {
                output[col][i] = " ";
            }

            for (int i = 0; i < colRestrictions[col].length; i++) {
                output[col][i + upPadding] = Integer.toString(colRestrictions[col][i]);
            }
        }

        return output;
    }

    private String buildRows(final int longestRowRestriction, final NonogramBoard nonogramBoard) {
        final List<String> rows = new ArrayList<>();

        for (int row = 0; row < nonogramBoard.getRows(); row++) {
            rows.add(buildRow(row, longestRowRestriction, nonogramBoard));
        }

        return String.join("\n", rows);
    }

    private String buildRow(final int row, final int longestRowRestriction, final NonogramBoard nonogramBoard) {
        final int[] rowRestrictions = nonogramBoard.getRowRestriction(row);

        final String rowRestrictionsSection = buildRowRestrictionSection(longestRowRestriction, rowRestrictions);

        final List<String> rowElements = new ArrayList<>();
        rowElements.add(rowRestrictionsSection);

        for (int col = 0; col < nonogramBoard.getCols(); col++) {
            if (nonogramBoard.isFilled(row, col)) {
                rowElements.add(FILLED);
            } else if (nonogramBoard.isBlocked(row, col)){
                rowElements.add(BLOCK);
            } else {
                rowElements.add(EMPTY);
            }
        }

        return String.join(" ", rowElements);
    }

    private String buildRowRestrictionSection(final int longestRowRestriction, final int[] rowRestrictions) {
        final int offset = longestRowRestriction - rowRestrictions.length;

        final String leftPadding = buildLeftPadding(offset);

        final List<String> rowRestrictionSectionElements = new ArrayList<>();
        if (leftPadding != null) {
            rowRestrictionSectionElements.add(leftPadding);
        }

        for (int i : rowRestrictions) {
            rowRestrictionSectionElements.add(Integer.toString(i));
        }

        return String.join(" ", rowRestrictionSectionElements);
    }

    private String buildLeftPadding(final int paddingSize) {
        if (paddingSize == 0) {
            return null;
        }

        final List<String> leftPadding = new ArrayList<>();

        for (int i = 0; i < paddingSize; i++) {
            leftPadding.add(" ");
        }

        return String.join(" ", leftPadding);
    }

    private int getLongestColRestriction(final NonogramBoard nonogramBoard) {
        int max = Integer.MIN_VALUE;

        for (int i = 0; i < nonogramBoard.getCols(); i++) {
            final int[] colRestrictions = nonogramBoard.getColRestriction(i);

            if (colRestrictions.length > max) {
                max = colRestrictions.length;
            }
        }

        return max;
    }

    private int getLongestRowRestriction(final NonogramBoard nonogramBoard) {
        int max = Integer.MIN_VALUE;

        for (int i = 0; i < nonogramBoard.getRows(); i++) {
            final int[] rowRestrictions = nonogramBoard.getRowRestriction(i);

            if (rowRestrictions.length > max) {
                max = rowRestrictions.length;
            }
        }

        return max;
    }

    public void print(final NonogramBoard nonogramBoard) {
        System.out.println(toStringRepresentation(nonogramBoard));
    }
}

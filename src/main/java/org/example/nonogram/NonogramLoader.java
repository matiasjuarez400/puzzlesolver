package org.example.nonogram;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.nonogram.board.NonogramBoard;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class NonogramLoader {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public NonogramBoard load(final Path file) throws IOException {
        final NonogramJsonModel model = OBJECT_MAPPER.readValue(file.toFile(), NonogramJsonModel.class);

        final NonogramBoard nonogramBoard = new NonogramBoard(model.getRows(), model.getCols());

        for (int row = 0; row < model.getRowRestrictions().size(); row++) {
            final List<Integer> currentRestrictions = model.getRowRestrictions().get(row);
            final int[] restrictionArray = currentRestrictions.stream()
                    .mapToInt(i -> i)
                    .toArray();

            nonogramBoard.addRowRestriction(row, restrictionArray);
        }

        for (int col = 0; col < model.getColRestrictions().size(); col++) {
            final List<Integer> currentRestrictions = model.getColRestrictions().get(col);
            final int[] restrictionArray = currentRestrictions.stream()
                    .mapToInt(i -> i)
                    .toArray();

            nonogramBoard.addColRestriction(col, restrictionArray);
        }

        return nonogramBoard;
    }
}

package org.example.nonogram.board;

import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NonogramBoard {
    private final SquareStatus[][] boardMatrix;
    private final SquareStatus[][] transposedMatrix;
    private final int[][] rowRestrictions;
    private final int[][] colRestrictions;

    public NonogramBoard(final int rows, final int cols) {
        this.boardMatrix = new SquareStatus[rows][cols];
        this.transposedMatrix = new SquareStatus[cols][rows];

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                boardMatrix[row][col] = SquareStatus.EMPTY;
                transposedMatrix[col][row] = SquareStatus.EMPTY;
            }
        }

        this.rowRestrictions = new int[rows][];
        this.colRestrictions = new int[cols][];
    }

    public void addRowRestriction(final int row, final int... restrictions) {
        this.rowRestrictions[row] = restrictions;
    }

    public void addColRestriction(final int col, final int... restrictions) {
        this.colRestrictions[col] = restrictions;
    }

    public boolean isFilled(final int row, final int col) {
        return hasStatus(SquareStatus.FILLED, row, col);
    }

    public boolean isEmpty(final int row, final int col) {
        return hasStatus(SquareStatus.EMPTY, row, col);
    }

    public boolean isBlocked(final int row, final int col) {
        return hasStatus(SquareStatus.BLOCKED, row, col);
    }

    private boolean hasStatus(final SquareStatus squareStatus, final int row, final int col) {
        return squareStatus.equals(boardMatrix[row][col]);
    }

    public void fill(final int row, final int col) {
        setStatus(row, col, SquareStatus.FILLED);
    }

    public void clear(final int row, final int col) {
        setStatus(row, col, SquareStatus.EMPTY);
    }

    public void block(final int row, final int col) {
        setStatus(row, col, SquareStatus.BLOCKED);
    }

    private void setStatus(final int row, final int col, final SquareStatus squareStatus) {
        this.boardMatrix[row][col] = squareStatus;
        this.transposedMatrix[col][row] = squareStatus;
    }

    public int getRows() {
        return boardMatrix.length;
    }

    public int getCols() {
        return boardMatrix[0].length;
    }

    public int[] getRowRestriction(final int row) {
        return rowRestrictions[row];
    }

    public int[] getColRestriction(final int col) {
        return colRestrictions[col];
    }

    public SquareStatus[] getCol(final int col) {
        return transposedMatrix[col];
    }

    public SquareStatus[] getRow(final int row) {
        return boardMatrix[row];
    }

    public BoardElements rowElements(final int row) {
        return new BoardElements(row, true, this.boardMatrix[row], getRowRestriction(row));
    }

    public BoardElements colElements(final int col) {
        return new BoardElements(col, false, this.transposedMatrix[col], getColRestriction(col));
    }

    public int countBlockedSquares() {
        return countSquaresWithStatus(SquareStatus.BLOCKED);
    }

    public int countFilledSquares() {
        return countSquaresWithStatus(SquareStatus.FILLED);
    }

    public int countEmptySquares() {
        return countSquaresWithStatus(SquareStatus.EMPTY);
    }

    public List<BoardElements> getAllBoardElements() {
        final List<BoardElements> boardElementsList = new ArrayList<>();

        for (int row = 0; row < this.getRows(); row++) {
            boardElementsList.add(this.rowElements(row));
        }

        for (int col = 0; col < this.getCols(); col++) {
            boardElementsList.add(this.colElements(col));
        }

        return boardElementsList;
    }

    public boolean isSolved() {
        final List<BoardElements> boardElementsList = getAllBoardElements();

        for (final BoardElements boardElements : boardElementsList) {
            final int totalRestrictions = Arrays.stream(boardElements.getRestrictions()).sum();

            int filledSquares = 0;
            for (int i = 0; i < boardElements.length(); i++) {
                if (boardElements.isFilled(i)) {
                    filledSquares++;
                }
            }

            if (filledSquares != totalRestrictions) {
                return false;
            }
        }

        final int emptySquares = countEmptySquares();

        return emptySquares == 0;
    }

    private int countSquaresWithStatus(final SquareStatus squareStatus) {
        int count = 0;

        for (int row = 0; row < this.getRows(); row++) {
            for (int col = 0; col < this.getCols(); col++) {
                if (hasStatus(squareStatus, row, col)) {
                    count++;
                }
            }
        }

        return count;
    }

    @ToString
    public class BoardElements {
        private final int row;
        private final int col;
        private final boolean isRow;
        private final SquareStatus[] elements;
        @Getter
        private final int[] restrictions;

        private BoardElements(int index, boolean isRow, final SquareStatus[] elements, final int[] restrictions) {
            if (isRow) {
                this.row = index;
                this.col = -1;
                this.isRow = true;
            } else {
                this.row = -1;
                this.col = index;
                this.isRow = false;
            }

            this.elements = elements;
            this.restrictions = restrictions;
        }

        public int length() {
            return elements.length;
        }

        public SquareStatus get(int index) {
            return elements[index];
        }

        public void fill(final int index) {
            this.setBoardStatus(index, SquareStatus.FILLED);
        }

        public boolean isEmpty(final int index) {
            return this.get(index).isEmpty();
        }

        public boolean isFilled(final int index) {
            return this.get(index).isFilled();
        }

        public void block(final int index) {
            this.setBoardStatus(index, SquareStatus.BLOCKED);
        }

        public void clear(final int index) {
            this.setBoardStatus(index, SquareStatus.EMPTY);
        }

        private void setBoardStatus(final int index, final SquareStatus squareStatus) {
            if (isRow) {
                setStatus(this.row, index, squareStatus);
            } else {
                setStatus(index, this.col, squareStatus);
            }
        }
    }
}

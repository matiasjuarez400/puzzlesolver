package org.example.nonogram.board;

public enum  SquareStatus {
    FILLED, EMPTY, BLOCKED;

    public boolean isEmpty() {
        return EMPTY.equals(this);
    }

    public boolean isBlocked() {
        return BLOCKED.equals(this);
    }

    public boolean isFilled() {
        return FILLED.equals(this);
    }
}

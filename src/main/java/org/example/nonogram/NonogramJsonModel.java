package org.example.nonogram;

import lombok.Data;

import java.util.List;

@Data
public class NonogramJsonModel {
    private int rows;
    private int cols;
    private List<List<Integer>> rowRestrictions;
    private List<List<Integer>> colRestrictions;
}

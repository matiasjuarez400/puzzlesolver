package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EightQueensSolverTest {
    @Test
    public void testCanInsertQueen() {
        EightQueensSolver solver = new EightQueensSolver();

        int[][] board = newBoard();

        board[0][0] = 1;

        assertFalse(solver.canInsertQueen(board, 0, 4));
        assertTrue(solver.canInsertQueen(board, 1, 4));

        assertFalse(solver.canInsertQueen(board, 7, 0));
        assertTrue(solver.canInsertQueen(board, 7, 1));

        assertFalse(solver.canInsertQueen(board, 7, 7));

        board[1][2] = 1;

        assertFalse(solver.canInsertQueen(board, 0, 4));
        assertFalse(solver.canInsertQueen(board, 1, 4));
        assertFalse(solver.canInsertQueen(board, 2, 3));
        assertFalse(solver.canInsertQueen(board, 2, 2));

        assertFalse(solver.canInsertQueen(board, 7, 0));
        assertTrue(solver.canInsertQueen(board, 7, 1));

        assertFalse(solver.canInsertQueen(board, 7, 7));
    }

    private int[][] newBoard() {
        return new int[8][8];
    }
}
package org.example.rotating_connections;

import org.junit.jupiter.api.Test;

import static org.example.rotating_connections.Color.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConnectionTest {
    @Test
    public void testConnectionRotation() {
        Connection connection = new Connection(new Color[][] {
                {R, P, P},
                {B, null, G},
                {G, B, R}
        });

        // ROTATION 1
        connection.rotate();

        Color[][] expectedResult = new Color[][] {
                {G, B, R},
                {B, null, P},
                {R, G, P}
        };

        assertEquals(1, connection.getRelativeRotation());
        validateRotationResult(connection, expectedResult);

        // ROTATION 2
        connection.rotate();

        expectedResult = new Color[][] {
                {R, B, G},
                {G, null, B},
                {P, P, R}
        };

        assertEquals(2, connection.getRelativeRotation());
        validateRotationResult(connection, expectedResult);

        // ROTATION 3
        connection.rotate();

        expectedResult = new Color[][] {
                {P, G, R},
                {P, null, B},
                {R, B, G}
        };

        assertEquals(3, connection.getRelativeRotation());
        validateRotationResult(connection, expectedResult);

        // ROTATION 4
        connection.rotate();

        expectedResult = new Color[][] {
                {R, P, P},
                {B, null, G},
                {G, B, R}
        };

        assertEquals(0, connection.getRelativeRotation());
        validateRotationResult(connection, expectedResult);
    }

    @Test
    public void testAntiRotate() {
        Connection connection = new Connection(new Color[][] {
                {R, P, P},
                {B, null, G},
                {G, B, R}
        });

        // ROTATION 1
        connection.rotate();

        Color[][] expectedResult = new Color[][] {
                {G, B, R},
                {B, null, P},
                {R, G, P}
        };

        validateRotationResult(connection, expectedResult);

        // ANTIROTATION 1
        connection.antiRotate();

        expectedResult = new Color[][] {
                {R, P, P},
                {B, null, G},
                {G, B, R}
        };

        assertEquals(0, connection.getRelativeRotation());
        validateRotationResult(connection, expectedResult);

        // ANTIROTATION 2
        connection.antiRotate();

        expectedResult = new Color[][] {
                {P, G, R},
                {P, null, B},
                {R, B, G}
        };

        assertEquals(3, connection.getRelativeRotation());
        validateRotationResult(connection, expectedResult);

        // ANTIROTATION 23
        connection.antiRotate();

        expectedResult = new Color[][] {
                {R, B, G},
                {G, null, B},
                {P, P, R}
        };

        assertEquals(2, connection.getRelativeRotation());
        validateRotationResult(connection, expectedResult);
    }

    private void validateRotationResult(Connection connection, Color[][] expectedResult) {
        for (int row = 0; row < expectedResult.length; row++) {
            for (int col = 0; col < expectedResult[0].length; col++) {
                assertEquals(expectedResult[row][col], connection.getConnection(row, col), String.format("Difference at [%s][%s]", row, col));
            }
        }
    }
}
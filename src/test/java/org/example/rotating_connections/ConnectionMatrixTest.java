package org.example.rotating_connections;

import org.junit.jupiter.api.Test;

import static org.example.rotating_connections.Color.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ConnectionMatrixTest {
    @Test
    public void testValidMatrix_withOneElement() {
        Connection connection = new Connection(new Color[][] {
                {R, B, G},
                {G, null, B},
                {P, P, R}
        });

        ConnectionConstraint constraint = new ConnectionConstraint(new Color[][] {
                {R, B, G},
                {G, null, null},
                {P, null, null}
        });

        ConnectionGroup connectionGroup = new ConnectionGroup(connection, constraint);

        ConnectionMatrix matrix = new ConnectionMatrix(1, 1);

        matrix.addRow(connectionGroup);

        assertTrue(matrix.isValidMatrix());

        matrix.rotate(0, 0);

        assertFalse(matrix.isValidMatrix());
    }

    @Test
    public void testValidMatrix_withTwoElements() {
        ConnectionGroup group1 = new ConnectionGroup(
                new Color[][]{
                        {R, B, G},
                        {G, null, B},
                        {P, P, R}
                },
                new Color[][]{
                        {R, B, G},
                        {G, null, null},
                        {P, null, null}
                });

        ConnectionGroup group2 = new ConnectionGroup(
                new Color[][]{
                        {R, B, P},
                        {B, null, C},
                        {C, P, R}
                },
                new Color[][]{
                        {R, B, P},
                        {null, null, null},
                        {null, null, null}
                });

        ConnectionMatrix matrix = new ConnectionMatrix(1, 2);

        matrix.addRow(group1, group2);

        assertTrue(matrix.isValidMatrix());

        matrix.rotate(0, 0);
        assertFalse(matrix.isValidMatrix());

        matrix.rotate(0, 1);
        assertFalse(matrix.isValidMatrix());

        matrix.rotate(0, 1);
        assertFalse(matrix.isValidMatrix());

        matrix.rotate(0, 0);
        assertTrue(matrix.isValidMatrix());
    }

    @Test
    public void testValidMatrix_withTwoRows_twoColumns() {
        ConnectionGroup group1 = new ConnectionGroup(
                new Color[][]{
                        {R, B, G},
                        {G, null, B},
                        {P, P, R}
                },
                new Color[][]{
                        {R, B, G},
                        {G, null, null},
                        {P, null, null}
                });

        ConnectionGroup group2 = new ConnectionGroup(
                new Color[][]{
                        {R, B, P},
                        {B, null, C},
                        {C, P, R}
                },
                new Color[][]{
                        {R, B, P},
                        {null, null, null},
                        {null, null, null}
                });

        ConnectionGroup group3 = new ConnectionGroup(
                new Color[][]{
                        {R, P, G},
                        {G, null, P},
                        {P, P, R}
                },
                new Color[][]{
                        {R, null, null},
                        {G, null, null},
                        {P, P, null}
                });

        ConnectionGroup group4 = new ConnectionGroup(
                new Color[][]{
                        {R, P, B},
                        {P, null, C},
                        {C, B, R}
                },
                new Color[][]{
                        {null, null, null},
                        {null, null, null},
                        {null, null, null}
                });

        ConnectionMatrix matrix = new ConnectionMatrix(2, 2);

        matrix.addRow(group1, group2);
        matrix.addRow(group3, group4);

        assertTrue(matrix.isValidMatrix());

        matrix.rotate(0, 0);
        assertFalse(matrix.isValidMatrix());

        matrix.rotate(0, 0);
        assertFalse(matrix.isValidMatrix());

        matrix.rotate(0, 0);
        assertFalse(matrix.isValidMatrix());

        matrix.rotate(0, 0);
        assertTrue(matrix.isValidMatrix());


        matrix.rotate(1, 1);
        assertFalse(matrix.isValidMatrix());

        matrix.rotate(1, 1);
        assertFalse(matrix.isValidMatrix());

        matrix.rotate(1, 1);
        assertFalse(matrix.isValidMatrix());

        matrix.rotate(1, 1);
        assertTrue(matrix.isValidMatrix());
    }
}
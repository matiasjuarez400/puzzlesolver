package org.example.rotating_connections;

import org.junit.jupiter.api.Test;

import static org.example.rotating_connections.Color.B;
import static org.example.rotating_connections.Color.G;
import static org.example.rotating_connections.Color.P;
import static org.example.rotating_connections.Color.R;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ConnectionGroupTest {
    @Test
    public void testConstraintAlignment() {
        Connection connection = new Connection(new Color[][] {
                {R, P, P},
                {B, null, G},
                {G, B, R}
        });

        ConnectionConstraint constraint = new ConnectionConstraint(new Color[][] {
                {R, B, G},
                {G, null, null},
                {P, null, null}
        });

        ConnectionGroup connectionGroup = new ConnectionGroup(connection, constraint);
        assertFalse(connectionGroup.isConstraintAligned());

        connectionGroup.rotateConnection();
        assertFalse(connectionGroup.isConstraintAligned());

        connectionGroup.rotateConnection();
        assertTrue(connectionGroup.isConstraintAligned());

        connectionGroup.rotateConnection();
        assertFalse(connectionGroup.isConstraintAligned());
    }
}
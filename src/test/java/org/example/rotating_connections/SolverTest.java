package org.example.rotating_connections;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.example.rotating_connections.Color.B;
import static org.example.rotating_connections.Color.C;
import static org.example.rotating_connections.Color.D;
import static org.example.rotating_connections.Color.G;
import static org.example.rotating_connections.Color.P;
import static org.example.rotating_connections.Color.R;
import static org.example.rotating_connections.Color.Y;

public class SolverTest {
    @Test
    public void testSolveMatrix() {
        ConnectionMatrix connectionMatrix = new ConnectionMatrix(3, 3);
        connectionMatrix.addRow(getFirstRow());
        connectionMatrix.addRow(getSecondRow());
        connectionMatrix.addRow(getThirdRow());

        Solver solver = new Solver();
        List<Coordinate> solution = solver.solve(connectionMatrix);

        for (Coordinate coordinate : solution) {
            System.out.println(coordinate);
        }
    }

    private ConnectionGroup[] getFirstRow() {
        ConnectionGroup group1 = new ConnectionGroup(
                new Color[][]{
                        {R, B, G},
                        {G, null, B},
                        {P, P, R}
                },
                new Color[][]{
                        {R, B, G},
                        {G, null, null},
                        {P, null, R}
                }, 2);

        ConnectionGroup group2 = new ConnectionGroup(
                new Color[][]{
                        {R, B, P},
                        {B, null, C},
                        {C, P, R}
                },
                new Color[][]{
                        {R, B, P},
                        {null, null, null},
                        {C, null, R}
                }, 0);

        ConnectionGroup group3 = new ConnectionGroup(
                new Color[][]{
                        {G, D, B},
                        {C, null, C},
                        {D, G, B}
                },
                new Color[][]{
                        {G, D, B},
                        {null, null, C},
                        {D, null, B}
                }, 0);

        return new ConnectionGroup[] {group1, group2, group3};
    }

    private ConnectionGroup[] getSecondRow() {
        ConnectionGroup group1 = new ConnectionGroup(
                new Color[][]{
                        {B, Y, C},
                        {P, null, B},
                        {C, P, Y}
                },
                new Color[][]{
                        {Y, null, C},
                        {B, null, null},
                        {C, null, B}
                }, 0);

        ConnectionGroup group2 = new ConnectionGroup(
                new Color[][]{
                        {G, P, R},
                        {D, null, P},
                        {R, G, D}
                },
                new Color[][]{
                        {R, null, D},
                        {null, null, null},
                        {G, null, R}
                }, 0);

        ConnectionGroup group3 = new ConnectionGroup(
                new Color[][]{
                        {B, C, R},
                        {G, null, C},
                        {R, G, B}
                },
                new Color[][]{
                        {R, null, B},
                        {null, null, C},
                        {B, null, R}
                }, 1);

        return new ConnectionGroup[] {group1, group2, group3};
    }

    private ConnectionGroup[] getThirdRow() {
        ConnectionGroup group1 = new ConnectionGroup(
                new Color[][]{
                        {P, G, B},
                        {Y, null, Y},
                        {B, P, G}
                },
                new Color[][]{
                        {B, null, G},
                        {G, null, null},
                        {P, Y, B}
                }, 0);

        ConnectionGroup group2 = new ConnectionGroup(
                new Color[][]{
                        {D, P, B},
                        {Y, null, D},
                        {P, Y, B}
                },
                new Color[][]{
                        {B, null, B},
                        {null, null, null},
                        {D, Y, P}
                }, 0);

        ConnectionGroup group3 = new ConnectionGroup(
                new Color[][]{
                        {D, Y, R},
                        {Y, null, C},
                        {C, R, D}
                },
                new Color[][]{
                        {R, null, D},
                        {null, null, R},
                        {D, Y, C}
                }, 1);

        return new ConnectionGroup[] {group1, group2, group3};
    }
}
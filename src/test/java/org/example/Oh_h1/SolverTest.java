package org.example.Oh_h1;

import org.example.Oh_h1.solution_seekers.SolutionSeeker;
import org.example.Oh_h1.solution_seekers.SolutionSeekersUtils;
import org.example.Oh_h1.solution_seekers.SurroundedBySameValueSolutionSeeker;
import org.example.Oh_h1.solution_seekers.TotalElementsInSeriesSolutionSeeker;
import org.example.Oh_h1.solution_seekers.TwoCellWithSameValueSolutionSeeker;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class SolverTest {
    @Test
    public void givenBoardWithSurroundedBySameValueAndTwoCellWithSameValueSeekers_whenSolve_theRightCellsAreFilled() {
        final List<SolutionSeeker> solutionSeekers = Arrays.asList(
                new TwoCellWithSameValueSolutionSeeker(),
                new SurroundedBySameValueSolutionSeeker()
        );

        final Solver solver = new Solver(solutionSeekers);

        final int[][] initialBoard = {
                {0, 1, 0, 2, 0, 2},
                {0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 0},
                {0, 0, 0, 1, 1, 0},
                {0, 2, 0, 0, 0, 0}
        };

        final Board board = new Board(initialBoard);

        final int[][] expectedBoard = {
                {0, 1, 0, 2, 1, 2},
                {0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 2, 0},
                {0, 2, 0, 0, 0, 0},
                {0, 1, 2, 1, 1, 2},
                {0, 2, 0, 0, 0, 0}
        };

        solver.solve(board);

        SolutionSeekersUtils.validateBoardHasValues(board, expectedBoard);
    }

    @Test
    public void givenBoardWithSurroundedBySameValueAndTwoCellWithSameAndTotalElementsInSeriesValueSeekers_whenSolve_theRightCellsAreFilled() {
        final List<SolutionSeeker> solutionSeekers = Arrays.asList(
                new TwoCellWithSameValueSolutionSeeker(),
                new SurroundedBySameValueSolutionSeeker(),
                new TotalElementsInSeriesSolutionSeeker()
        );

        final Solver solver = new Solver(solutionSeekers);

        final int[][] initialBoard = {
                {0, 1, 0, 2, 0, 2},
                {0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 0},
                {0, 0, 0, 1, 1, 0},
                {0, 2, 0, 0, 0, 0}
        };

        final Board board = new Board(initialBoard);

        final int[][] expectedBoard = {
                {0, 1, 0, 2, 1, 2},
                {0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 2, 0},
                {0, 2, 0, 0, 2, 0},
                {2, 1, 2, 1, 1, 2},
                {0, 2, 0, 0, 2, 0}
        };

        solver.solve(board);

        SolutionSeekersUtils.validateBoardHasValues(board, expectedBoard);
    }
}
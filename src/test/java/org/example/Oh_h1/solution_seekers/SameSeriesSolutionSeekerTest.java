package org.example.Oh_h1.solution_seekers;

import org.example.Oh_h1.Board;
import org.junit.jupiter.api.Test;

class SameSeriesSolutionSeekerTest {
    @Test
    public void givenBoard_whenSeek_theRightCellsAreFilled() {
        final SolutionSeeker seeker = new SameSeriesSolutionSeeker();

        final int[][] initialBoard = {
                {0, 1, 0, 2, 1, 2},
                {0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 2, 0},
                {0, 2, 0, 0, 2, 0},
                {2, 1, 2, 1, 1, 2},
                {0, 2, 0, 0, 2, 0}
        };

        final Board board = new Board(initialBoard);

        final int[][] expectedBoard = {
                {0, 1, 0, 2, 1, 2},
                {0, 2, 0, 0, 1, 0},
                {1, 1, 0, 0, 2, 0},
                {0, 2, 0, 0, 2, 0},
                {2, 1, 2, 1, 1, 2},
                {0, 2, 0, 0, 2, 0}
        };

        seeker.seek(board);

        SolutionSeekersUtils.validateBoardHasValues(board, expectedBoard);
    }
}
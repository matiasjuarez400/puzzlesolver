package org.example.Oh_h1.solution_seekers;

import org.example.Oh_h1.Board;
import org.junit.jupiter.api.Test;

public class TwoCellWithSameValueSolutionSeekerTest {
    @Test
    public void givenBoard_fillSomeCells() {
        final int[][] initialBoard = {
                {0, 1, 0, 2, 0, 2},
                {0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 0},
                {0, 0, 0, 1, 1, 0},
                {0, 2, 0, 0, 0, 0}
        };

        final Board board = new Board(initialBoard);

        final SolutionSeeker seeker = new TwoCellWithSameValueSolutionSeeker();
        seeker.seek(board);

        final int[][] expectedBoard = {
                {0, 1, 0, 2, 0, 2},
                {0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 0},
                {0, 0, 2, 1, 1, 2},
                {0, 2, 0, 0, 0, 0}
        };

        SolutionSeekersUtils.validateBoardHasValues(board, expectedBoard);
    }
}
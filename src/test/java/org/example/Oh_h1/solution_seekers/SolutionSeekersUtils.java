package org.example.Oh_h1.solution_seekers;

import org.example.Oh_h1.Board;

public class SolutionSeekersUtils {
    public static void validateBoardHasValues(final Board board, final int[][] expectedValues) {
        if (board.getSize() != expectedValues.length || board.getSize() != expectedValues[0].length) {
            throw new IllegalArgumentException("ExpectedValues matrix must have the same size as the Board");
        }

        for (int row = 0; row < board.getSize(); row++) {
            for (int col = 0; col < board.getSize(); col++) {
                final int boardValue = board.getCell(row, col);
                final int expectedValue = expectedValues[row][col];

                if (boardValue != expectedValue) {
                    final String model = "The Board value [%d] is different from expected value [%s] at row [%d] - col [%d]";
                    final String msg = String.format(model, boardValue, expectedValue, row, col);
                    throw new IllegalStateException(msg);
                }
            }
        }
    }
}

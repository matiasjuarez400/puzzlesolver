package org.example.monster_hunter;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MonsterHunterSolverTest {
    @Test
    public void testLevel1() {
        int[][] level = new int[][] {
                {0, 2, 0, 0, 0, 2, 0},
                {0, 2, 0, 2, 2, 2, 2},
                {0, 0, 0, 2, 0, 0, 0},
                {0, 2, 0, 2, 0, 2, 0},
                {0, 0, 1, 0, 0, 0, 0},
                {0, 2, 0, 2, 2, 2, 0},
                {0, 0, 0, 0, 0, 0, 0}
        };

        String expected = "{2-0}-{0-2}-{2-4}-{4-6}-{6-4}-{4-2}-{0-5}-{2-5}-{1-6}-{3-4}-{1-2}-{0-1}-{2-3}-{1-4}-{3-2}-{3-3}-{3-1}-{5-3}-{5-4}-{5-2}-{5-0}";
        String solution = getSolution(level);

        assertEquals(expected, solution);
    }

    @Test
    public void testLevel2() {
        int[][] level = new int[][] {
                {0, 0, 0, 2, 0, 2, 0},
                {1, 2, 0, 2, 2, 2, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0},
                {0, 2, 0, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 2, 0},
                {2, 0, 2, 0, 0, 2, 0}
        };

        String expected = "{1-2}-{0-3}-{2-5}-{4-3}-{0-5}-{2-5}-{6-0}-{4-2}-{6-5}-{4-5}-{1-2}-{1-4}-{3-6}-{5-4}-{3-2}-{5-2}-{4-1}-{6-3}-{6-1}";
        String solution = getSolution(level);

        assertEquals(expected, solution);
    }

    @Test
    public void testLevel3() {
        int[][] level = new int[][] {
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 0, 0},
                {0, 2, 0, 2, 0, 2, 0},
                {0, 2, 0, 0, 0, 2, 0},
                {0, 0, 0, 0, 2, 0, 2},
                {0, 0, 0, 2, 2, 0, 0},
                {0, 0, 0, 0, 0, 0, 0}
        };

        String expected = "{3-4}-{3-6}-{2-1}-{4-1}-{4-6}-{2-6}-{2-4}-{5-3}-{5-5}-{3-3}-{2-4}-{4-2}-{4-0}";
        String solution = getSolution(level);

        assertEquals(expected, solution);
    }

    private String getSolution(int[][] level) {
        MonsterHunterSolver solver = new MonsterHunterSolver();
        List<SolutionMove> solutionMoves = solver.solve(level);

        return solutionMoves.stream()
                .map(SolutionMove::toString)
                .collect(Collectors.joining("-"));
    }

    private void displaySolution(int[][] level) {
        MonsterHunterSolver solver = new MonsterHunterSolver();

        List<SolutionMove> solution = solver.solve(level);

        System.out.println("Found solution. Steps: " + solution.size());

        int i = 1;
        for (SolutionMove solutionMove : solution) {
            System.out.println(i++ + ")" + solutionMove);
        }
    }
}